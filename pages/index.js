import React from 'react';
import client from '../src/apollo/client';

import SEO from '../src/components/common/seo';
import Layout from '../src/components/layout';
import RenderHomePage from '../src/pages/homePage';
import axios from 'axios';
import { GetACityForHomePage } from '../src/apollo/queries/GetACityForHomePage';
export default function Home(props) {
  var HomePageData = [];

  var cities = ['hyderabad', 'chennai', 'mumbai', 'delhi', 'bangalore'];

  for (const city of props.HomePageData) {
    var newsletters = city.cityData
      ? city.cityData?.newsletters?.edges?.map(({ node: newsletter }) => {
          return {
            title: newsletter.title || '',
            slug: newsletter.slug || '',
            date: newsletter.dateGmt || '',
            imageUrl: newsletter?.featuredImage?.node?.mediaItemUrl || '',
          };
        })
      : [];

    var stories = city.cityData
      ? city.cityData?.stories?.edges?.map(({ node: story }) => {
          return {
            title: story.title || '',
            slug: story.slug || '',
            date: story.dateGmt || '',
            imageUrl: story?.featuredImage?.node?.mediaItemUrl || '',
            authorName: story?.author?.node?.name || '',
            category: story?.categories?.edges
              ? story?.categories?.edges[0]?.node?.name || ''
              : ' ',
          };
        })
      : [];

    var citySpeaks = city.cityData
      ? city.cityData?.cityspeaks?.edges?.map(({ node: cityspeak }) => {
          return {
            title: cityspeak.title || '',
            slug: cityspeak.slug || '',
            date: cityspeak.dateGmt || '',
            imageUrl: cityspeak?.featuredImage?.node?.mediaItemUrl || '',
            authorName: cityspeak?.author?.node?.name || '',
          };
        })
      : [];

    HomePageData.push({
      city: city.city,
      newsletters: newsletters,
      stories: stories,
      citySpeaks: citySpeaks,
    });
  }
  return (
    <div>
      <SEO
        description="Discover your city with Local News and updates delivered to your email everyday morning at 7am by Nearby News"
        uri="https://www.nearbynews.in"
        title="Metro Cities and Local News | Nearby News"
        image="https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png"
      />
      <Layout>
        <RenderHomePage HomePageData={HomePageData} />
      </Layout>
    </div>
  );
}

export async function getStaticProps() {
  const { performance } = require('perf_hooks');
  const t0 = performance.now();

  var HomePageData = [];

  var count = 10;
  var cities = ['hyderabad', 'chennai', 'mumbai', 'delhi', 'bangalore'];
  for (const city of cities) {
    var { data: cityData, errors: errorsNLs } = await client.query({
      query: GetACityForHomePage,
      variables: { city, count },
    });
    HomePageData.push({
      city: city,
      cityData: cityData,
    });
  }

  const t1 = performance.now();
  console.log(`Home page  took ${((t1 - t0) / 1000).toFixed(2)} Seconds.`);
  return {
    props: { HomePageData: HomePageData },
    revalidate: 300,
  };
}
