import { useRouter } from 'next/router';
import React from 'react';
import client from '../../../../src/apollo/client';
import { GetCities } from '../../../../src/apollo/queries/getCities';
import { GetStoriesOfCategory } from '../../../../src/apollo/queries/getStoriesOfCategory';
import SEO from '../../../../src/components/common/seo';
import Layout from '../../../../src/components/layout';
import ArchivePage from '../../../../src/pages/archive';
import { toPascalCase } from '../../../../utils/functions';

export default function Stories(props) {
  const { city, dataStories } = props;
  const router = useRouter();

  const stories = dataStories
    ? dataStories?.stories?.edges?.map(({ node: story }) => {
        return {
          title: story.title || '',
          slug: story.slug || '',
          date: story.dateGmt || '',
          imageUrl: story?.featuredImage?.node?.mediaItemUrl || '',
          authorName: story?.author?.node?.name || '',
          category: story.categories.edges
            ? story?.categories?.edges[0]?.node?.name || ''
            : '',
        };
      })
    : [];
  const { category } = router.query;
  return (
    <Layout>
      <SEO
        description={`Get top stories and articles on happenings around ${toPascalCase(
          city,
        )}`}
        uri={`https://www.nearbynews.in/${city}/stories`}
        title={`${toPascalCase(city)}  Articles & Stories  | Nearby News`}
        image="https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png"
      />
      <ArchivePage
        section={category?.replace(/(\w)(\w*)/g, function (g0, g1, g2) {
          return g1.toUpperCase() + g2.toLowerCase();
        })}
        sectionSlug="stories"
        city={city}
        items={stories}
        Bgcolor="white"
      />
    </Layout>
  );
}

export async function getStaticPaths() {
  const { data } = await client.query({
    query: GetCities,
  });

  const categories = [
    'lifestyle',
    'business',
    'administration',
    'politics',
    'entertainment',
    'startups',
  ];
  let cityParams = [];
  data?.cities?.edges?.forEach(({ node: city }) => {
    categories.forEach((element) => {
      cityParams.push({ params: { city: city.slug, category: element } });
    });
    return cityParams;
  });

  return { paths: cityParams, fallback: true };
}

export async function getStaticProps({ params }) {
  const { performance } = require('perf_hooks');
  const t0 = performance.now();
  const { city: city, category: category } = params;
  const count = 100;

  const { data: dataStories, errors: errorsStories } = await client.query({
    query: GetStoriesOfCategory,
    variables: { city, category, count },
  });
  const t1 = performance.now();
  console.log(`Cat page  took ${((t1 - t0) / 1000).toFixed(2)} Seconds.`);
  return { props: { city: params.city, dataStories: dataStories }, revalidate: 60 };
}
