import { useRouter } from 'next/router';
import React from 'react';
import client from '../../../src/apollo/client';
import { GetCities } from '../../../src/apollo/queries/getCities';
import { GetStoriesOfCity } from '../../../src/apollo/queries/getStoriesOfCity';
import SEO from '../../../src/components/common/seo';
import Layout from '../../../src/components/layout';
import ArchivePage from '../../../src/pages/archive';
import { toPascalCase } from '../../../utils/functions';
export default function Stories(props) {
  const { city, dataStories } = props;
  const router = useRouter();

  const stories = dataStories
    ? dataStories?.stories?.edges?.map(({ node: story }) => {
        return {
          title: story.title || '',
          slug: story.slug || '',
          date: story.dateGmt || '',
          imageUrl: story?.featuredImage?.node?.mediaItemUrl || '',
          authorName: story?.author?.node?.name || '',
          category: story.categories.edges
            ? story?.categories?.edges[0]?.node?.name || ''
            : '',
        };
      })
    : [];

  return (
    <Layout>
      <SEO
        description={`Get top stories and articles on happenings around ${toPascalCase(
          city,
        )}`}
        uri={`https://www.nearbynews.in/${city}/stories`}
        title={`${toPascalCase(city)}  Articles & Stories  | Nearby News`}
        image="https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png"
      />
      <ArchivePage
        section="Stories"
        sectionSlug="stories"
        city={city}
        items={stories}
        Bgcolor="white"
        description="Remember the experience of sipping a coffee and reading a local newspaper in the early morning? We would like to bring back the experience for you with a digital touch and zero spam."
      />
    </Layout>
  );
}

export async function getStaticPaths() {
  const { data } = await client.query({
    query: GetCities,
  });

  const citypaths = data
    ? data?.cities?.edges?.map(({ node: city }) => {
        return { params: { city: city.slug } };
      })
    : [];

  return { paths: citypaths, fallback: true };
}

export async function getStaticProps({ params }) {
  const { performance } = require('perf_hooks');
  const t0 = performance.now();
  const { city: city } = params;
  const count = 100;

  const { data: dataStories } = await client.query({
    query: GetStoriesOfCity,
    variables: { city, count },
  });

  const t1 = performance.now();
  console.log(`Stories  page  took ${((t1 - t0) / 1000).toFixed(2)} Seconds.`);

  return { props: { city: params.city, dataStories: dataStories }, revalidate: 60 };
}
