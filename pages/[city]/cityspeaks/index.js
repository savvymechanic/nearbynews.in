import { useRouter } from 'next/router';
import React from 'react';
import client from '../../../src/apollo/client';
import { GetCities } from '../../../src/apollo/queries/getCities';
import { GetCitySpeaks } from '../../../src/apollo/queries/getCitySpeaks';
import SEO from '../../../src/components/common/seo';
import Layout from '../../../src/components/layout';
import ArchivePage from '../../../src/pages/archive';
import { toPascalCase } from '../../../utils/functions';

export default function CitySpeaksPage(props) {
  const { city: city, dataCitySpeaks: dataCitySpeaks } = props;

  const citySpeaks = dataCitySpeaks
    ? dataCitySpeaks?.cityspeaks?.edges?.map(({ node: cityspeak }) => {
        return {
          title: cityspeak.title || '',
          slug: cityspeak.slug || '',
          date: cityspeak.dateGmt || '',
          imageUrl: cityspeak?.featuredImage?.node?.mediaItemUrl || '',
          authorName: cityspeak?.author?.node?.name || '',
        };
      })
    : [];

  const router = useRouter();
  return (
    <div>
      <SEO
        description={`Get Problems and opinions around ${toPascalCase(city)}`}
        uri={`https://www.nearbynews.in/${city}/cityspeaks`}
        title={`${toPascalCase(city)}  City Speaks | Nearby News`}
        image="https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png"
      />
      <Layout>
        <ArchivePage
          section="City Speaks"
          sectionSlug="cityspeaks"
          city={city}
          items={citySpeaks}
          Bgcolor="white"
          description="Remember the experience of sipping a coffee and reading a local newspaper in the early morning? We would like to bring back the experience for you with a digital touch and zero spam."
        />
      </Layout>
    </div>
  );
}

export async function getStaticPaths() {
  const { data } = await client.query({
    query: GetCities,
  });

  const citypaths = data
    ? data?.cities?.edges?.map(({ node: city }) => {
        return { params: { city: city.slug } };
      })
    : [];

  return { paths: citypaths, fallback: true };
}

export async function getStaticProps({ params }) {
  const { performance } = require('perf_hooks');
  const t0 = performance.now();

  const { city: city } = params;
  const count = 100;

  const { data: dataCitySpeaks, errors: errorsStories } = await client.query({
    query: GetCitySpeaks,
    variables: { city, count },
  });

  dataCitySpeaks
    ? console.log(`${city}'s City Speaks fetched`)
    : console.error(`${city}'s CitySpeaks Failed :`, errorsStories);
  const t1 = performance.now();
  console.log(`cityspeaks page  took ${((t1 - t0) / 1000).toFixed(2)} Seconds.`);
  return {
    props: { city: params.city, dataCitySpeaks: dataCitySpeaks },
    revalidate: 300,
  };
}
