import { useRouter } from 'next/router';
import React from 'react';
import client from '../../../src/apollo/client';
import { GetCities } from '../../../src/apollo/queries/getCities';
import { GetNLsOfCity } from '../../../src/apollo/queries/getNLsOfCity';
import SEO from '../../../src/components/common/seo';
import Layout from '../../../src/components/layout';
import ArchivePage from '../../../src/pages/archive/';
import { toPascalCase } from '../../../utils/functions';

export default function Newsletters(props) {
  const router = useRouter(props);
  const { city, dataNLs } = props;

  const newsletters = dataNLs
    ? dataNLs?.newsletters?.edges?.map(({ node: newsletter }) => {
        return {
          title: newsletter.title || '',
          slug: newsletter.slug || '',
          date: newsletter.dateGmt || '',
          imageUrl: newsletter?.featuredImage?.node?.mediaItemUrl || '',
          authorName: newsletter?.author?.node?.name || '',
        };
      })
    : [];

  return (
    <Layout>
      <SEO
        description={` ${toPascalCase(city)} Newsletters | Nearby News`}
        uri={`https://www.nearbynews.in/${city}/newsletters`}
        title={`Compilation of all our local news letters of ${toPascalCase(city)}`}
        image="https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png"
      />
      <ArchivePage
        section="Newsletters"
        sectionSlug="newsletters"
        city={city}
        items={newsletters}
        Bgcolor="white"
        description="Remember the experience of sipping a coffee and reading a local newspaper in the early morning? We would like to bring back the experience for you with a digital touch and zero spam."
      />
    </Layout>
  );
}

export async function getStaticPaths() {
  const { data } = await client.query({
    query: GetCities,
  });

  const citypaths = data
    ? data?.cities?.edges?.map(({ node: city }) => {
        return { params: { city: city.slug } };
      })
    : [];

  return { paths: citypaths, fallback: true };
}

export async function getStaticProps({ params }) {
  const { performance } = require('perf_hooks');
  const t0 = performance.now();
  const { city: city } = params;
  const count = 100;

  const { data: dataNLs, errors: errorsNLs } = await client.query({
    query: GetNLsOfCity,
    variables: { city, count },
  });

  dataNLs
    ? console.log(`${city}'s NLs fetched`)
    : console.log(`${city}'s NLs Failed :`, errorsNLs);

  const t1 = performance.now();
  console.log(`Newsletters page  took ${((t1 - t0) / 1000).toFixed(2)} Seconds.`);

  return {
    props: { city: params.city, dataNLs: dataNLs },
    revalidate: 300,
  };
}
