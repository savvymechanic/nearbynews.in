import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ApolloProvider } from '@apollo/client';
import client from '../src/apollo/client';
import theme, { darkTheme, lightTheme } from '../styles/theme';
import { responsiveFontSizes } from '@material-ui/core/styles';
import NextNprogress from 'nextjs-progressbar';
import redirect from 'nextjs-redirect';
import { useRouter } from 'next/router';

export default function MyApp(props) {
  const router = useRouter();
  if (!(typeof window === 'undefined')) {
    const windowWidth = window.innerWidth;
    // if (windowWidth < 500) {
    //   router.push('https://www.m.nearbynews.in/');
    //   // redirect('https://www.m.nearbynews.in/', { statusCode: 302 });
    // }
  }

  const { Component, pageProps } = props;
  //const { value: isDark } = useDarkMode();
  const themeConfig = lightTheme; // isDark ? darkTheme : lightTheme;

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>Nearby News</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>
      <ThemeProvider theme={responsiveFontSizes(themeConfig)}>
        <ApolloProvider client={client}>
          <CssBaseline />
          <NextNprogress
            options={{ easing: 'ease', speed: 500 }}
            ya
            color="#29D"
            startPosition={0.3}
            stopDelayMs={200}
            height="3"
          />
          <Component {...pageProps} />
        </ApolloProvider>
      </ThemeProvider>
    </React.Fragment>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};
