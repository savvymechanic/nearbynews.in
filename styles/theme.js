import { grey } from '@material-ui/core/colors';
import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

// Create a theme instance.

let theme = {
  typography: {
    fontFamily: ['Roboto'].join(','),
  },
  palette: {
    primary: {
      main: '#0988C8',
    },
    secondary: {
      main: '#f1a104;',
    },

    typography: {
      button: {
        textTransform: 'none',
      },
    },
  },
};

//theme = responsiveFontSizes(theme);

export const darkTheme = createMuiTheme({
  ...theme,
  palette: {
    ...theme.palette,
    type: 'dark',
    // background:{
    //   paper:grey[900],
    //   default:grey[900]
    // }
  },
});

export const lightTheme = createMuiTheme({
  ...theme,
  palette: {
    ...theme.palette,
    type: 'light',
  },
});
