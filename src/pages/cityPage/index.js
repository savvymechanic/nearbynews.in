import { Container, Typography, Grid, Box, Paper, CardMedia } from '@material-ui/core';
import React from 'react';
import ItemSection from '../../components/common/itemSection';
import Image from 'next/image';
import { motion } from 'framer-motion';
import Link from 'next/link';
import { Twitter, Facebook, Linkedin, Mail, Whatsapp } from 'react-social-sharing';
import { useRouter } from 'next/router';
import Carousel from 'react-material-ui-carousel';

const RenderCityPage = (props) => {
  const { city, newsletters, stories, citySpeaks } = props;

  return (
    <Container>
      {/* <Typography variant="h1"> {city}&apos;s Page </Typography> */}
      {CityStoriesGrid(stories, city)}
      <ItemSection
        items={newsletters}
        city={city}
        section="Newsletters"
        sectionSlug="newsletters"
        Bgcolor="#16418f"
        as={`/${city}/newsletters`}
        href="/[city]/newsletters"
        coloums={3}
        noOfItems={4}
      />
      <Grid container style={{ padding: '1.5rem 0rem' }}>
        <Grid item md={8}>
          <ItemSection
            items={citySpeaks}
            city={city}
            section="Cityspeaks"
            sectionSlug="cityspeaks"
            Bgcolor="#16418f"
            as={`/${city}/cityspeaks`}
            href="/[city]/cityspeaks"
            coloums={6}
            noOfItems={2}
          />
          <Grid container>
            <Grid item md={6}>
              {VerticalCitySpeaks(citySpeaks?.slice(0, 2), city)}
            </Grid>
            <Grid item md={6}>
              {VerticalCitySpeaks(citySpeaks?.slice(0, 2), city)}
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={3} style={{ padding: '1.5rem 0rem' }}>
          <Image
            src={
              'https://res.cloudinary.com/nearbynews/image/upload/v1606747679/Vertical-Here_zcvt0y.png'
            }
            alt="ads"
            layout="intrinsic"
            width="400px"
            height="600px"
          />
        </Grid>
      </Grid>

      <Grid container style={{ padding: '1.5rem 0rem' }}>
        <Grid item md={8}>
          <Box display="flex">
            <ItemSection
              items={stories}
              city={city}
              section="Politics"
              sectionSlug="stories"
              Bgcolor="#16418f"
              as={`/${city}/stories/politics`}
              href="/[city]/stories/politics"
              coloums={12}
              noOfItems={1}
            />
            <Box mt={5}>{VerticalCitySpeaks(stories?.slice(0, 5), city)}</Box>
          </Box>
        </Grid>
        <Grid item md={3} style={{ padding: '1.5rem 0rem' }}>
          {VerticalStayConnected(stories?.slice(0, 7), city)}
        </Grid>
      </Grid>

      <Grid container style={{ padding: '1.5rem 0rem' }}>
        <Grid item md={8}>
          <ItemSection
            items={stories}
            city={city}
            section="Administration"
            sectionSlug="stories"
            Bgcolor="#16418f"
            as={`/${city}/stories/administration`}
            href="/[city]/stories/administration"
            coloums={6}
            noOfItems={4}
          />
        </Grid>
        <Grid item md={3} style={{ padding: '1.5rem 0rem' }}>
          {VerticalMostPopular(stories?.slice(0, 7), city)}
        </Grid>
      </Grid>
      {/* <StoriesSection stories={stories} city={city} />
      <NewsLettersSection newsletters={newsletters} city={city} />
      <CitySpeaksSection citySpeaks={citySpeaks} city={city} /> */}
    </Container>
  );
};

RenderCityPage.propTypes = {};

export default RenderCityPage;

const VerticalCitySpeaks = (highLights, city) => {
  return (
    <Box mt={4} mr={3} textAlign="center">
      {highLights?.map((highLight) => {
        return (
          <motion.div
            whileHover={{ scale: 1.05 }}
            whileTap={{ scale: 0.8, borderRadius: '100%' }}
            animate={{ scale: 1 }}
            transformTemplate={(props, transform) =>
              // Disable GPU acceleration to prevent blurry text
              transform.replace(' translateZ(0)', '')
            }
            key={highLight.slug}
          >
            <Box my={3} display="flex" key={highLight.slug} textAlign="left" px={7}>
              <Box display="flex">
                <Link
                  as={`/${city}/stories/${highLight.slug}`}
                  href={`/[city]/stories/[slug]`}
                  prefetch={false}
                >
                  <Box fontWeight={700} display="flex" style={{ cursor: 'pointer' }}>
                    <Image
                      src={
                        highLight.imageUrl ||
                        'https://res.cloudinary.com/nearbynews/image/upload/v1606747679/Vertical-Here_zcvt0y.png'
                      }
                      alt={highLight.title}
                      layout="fixed"
                      width="100px"
                      height="auto"
                      quality={100}
                    />

                    <Box ml={1} fontSize="0.8rem">
                      {highLight.title}
                    </Box>
                  </Box>
                </Link>
              </Box>
              {/* </li> */}
            </Box>
          </motion.div>
        );
      })}
    </Box>
  );
};

const VerticalMostPopular = (highLights, city) => {
  return (
    <Box mt={-1} mr={3} textAlign="center">
      <Box
        style={{
          borderBottom: `2px solid #f9c100`,
          marginRight: '10px',
          marginLeft: '10px',
        }}
      >
        <Box
          style={{
            backgroundColor: `#f9c100`,
            color: ' #16418f',
            fontFamily: 'Roboto, sans-serif',
            fontSize: '14px',
            fontWeight: 800,
            height: '27px',
            lineHeight: '17px',
            margin: '0px',
            padding: '7px 12px 4px',
            width: '121px',
            marginBottom: '-2px',
          }}
        >
          Most Popular
        </Box>
      </Box>
      {highLights?.map((highLight) => {
        return (
          <motion.div
            whileHover={{ scale: 1.05 }}
            whileTap={{ scale: 0.8, borderRadius: '100%' }}
            animate={{ scale: 1 }}
            transformTemplate={(props, transform) =>
              // Disable GPU acceleration to prevent blurry text
              transform.replace(' translateZ(0)', '')
            }
            key={highLight.slug}
          >
            <Box my={3} display="flex" key={highLight.slug} textAlign="left" px={1}>
              <Box display="flex">
                <Link
                  as={`/${city}/stories/${highLight.slug}`}
                  href={`/[city]/stories/[slug]`}
                  prefetch={false}
                >
                  <Box fontWeight={700} display="flex" style={{ cursor: 'pointer' }}>
                    <Image
                      src={
                        highLight.imageUrl ||
                        'https://res.cloudinary.com/nearbynews/image/upload/v1606747679/Vertical-Here_zcvt0y.png'
                      }
                      alt={highLight.title}
                      layout="fixed"
                      width="100px"
                      height="70px"
                      quality={100}
                    />

                    <Box ml={1} fontSize="0.8rem">
                      {highLight.title}
                    </Box>
                  </Box>
                </Link>
              </Box>
              {/* </li> */}
            </Box>
          </motion.div>
        );
      })}
    </Box>
  );
};

const VerticalStayConnected = (highLights, city) => {
  const router = useRouter();

  return (
    <Box mt={-1} mr={3} textAlign="center">
      <Box
        style={{
          borderBottom: `2px solid #f9c100`,
          marginRight: '10px',
          marginLeft: '10px',
        }}
      >
        <Box
          style={{
            backgroundColor: `#f9c100`,
            color: ' #16418f',
            fontFamily: 'Roboto, sans-serif',
            fontSize: '14px',
            fontWeight: 800,
            height: '27px',
            lineHeight: '17px',
            margin: '0px',
            padding: '7px 12px 4px',
            width: '121px',
            marginBottom: '-2px',
          }}
        >
          Share
        </Box>
      </Box>
      <Box display="flex" flexWrap="wrap" pl={1} pt={2}>
        <Whatsapp link={`nearbynews.in/${router.asPath}`} />
        <Twitter link={`nearbynews.in/${router.asPath}`} />
        <Facebook link={`nearbynews.in/${router.asPath}`} />
        <Linkedin link={`nearbynews.in/${router.asPath}`} />
      </Box>
      <Box pt={5} pl={2} display="flex">
        <Image
          src={
            'https://res.cloudinary.com/nearbynews/image/upload/v1606747679/Vertical-Here_zcvt0y.png'
          }
          alt="ads"
          layout="intrinsic"
          width="200px"
          height="200px"
        />
      </Box>
    </Box>
  );
};

const CityStoriesGrid = (stories, city) => {
  var items = [
    {
      name: 'Random Name #1',
      imageUrl:
        'https://res.cloudinary.com/nearbynews/image/upload/v1607109667/HUSSAINSAGAR_rotdo2.jpg',
    },
    {
      name: 'Random Name #2',
      imageUrl:
        'https://res.cloudinary.com/nearbynews/image/upload/v1607109670/Five-posh-areas-in-Hyderabad-FB-1200x700-compressed_smih9s.jpg',
    },
    {
      name: 'Random Name #2',
      imageUrl:
        'https://res.cloudinary.com/nearbynews/image/upload/v1607109671/images_cvtrmj.jpg',
    },
  ];
  return (
    <Box>
      <Grid container>
        <Grid item md={6}>
          <ItemSection
            items={stories}
            city={city}
            section="Politics"
            sectionSlug="stories"
            Bgcolor="#16418f"
            as={`/${city}/stories/administration`}
            href="/[city]/stories/administration"
            coloums={12}
            noOfItems={1}
            isArchive={true}
          />
          <Grid container>
            <Grid item md={6}>
              <ItemSection
                items={stories}
                city={city}
                section="Politics"
                sectionSlug="stories"
                Bgcolor="#16418f"
                as={`/${city}/stories/administration`}
                href="/[city]/stories/administration"
                coloums={12}
                noOfItems={1}
                isArchive={true}
              />
            </Grid>
            <Grid item md={6}>
              <ItemSection
                items={stories}
                city={city}
                section="Politics"
                sectionSlug="stories"
                Bgcolor="#16418f"
                as={`/${city}/stories/administration`}
                href="/[city]/stories/administration"
                coloums={12}
                noOfItems={1}
                isArchive={true}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={6}>
          <ItemSection
            items={stories}
            city={city}
            section="Politics"
            sectionSlug="stories"
            Bgcolor="#16418f"
            as={`/${city}/stories/administration`}
            href="/[city]/stories/administration"
            coloums={12}
            noOfItems={1}
            isArchive={true}
          />
          <Box px={4} mt={4}>
            <Carousel
              fullHeightHover={true}
              animation="slide"
              indicators={false}
              navButtonsAlwaysVisible={true}
            >
              {items.map((item, i) => (
                <Box style={{ height: '17rem' }}>
                  <Image
                    src={
                      item.imageUrl ||
                      'https://res.cloudinary.com/nearbynews/image/upload/v1606747679/Vertical-Here_zcvt0y.png'
                    }
                    alt={item.title}
                    layout="fill"
                    objectFit="cover"
                    // width="100%"
                    //height="100%"
                    quality={100}
                  />
                </Box>
              ))}
            </Carousel>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};
