import { Box, Container, Typography, makeStyles } from '@material-ui/core';
import React from 'react';
import ItemSection from '../../components/common/itemSection';
import { toPascalCase } from '../../../utils/functions/stringManipulation';

const ArchivePage = (props) => {
  const useStyles = makeStyles((theme) => ({
    title: {
      color: '#f1a104',
      fontFamily: 'roboto-bold, roboto, sans-serif',
      fontSize: '28px',
      fontWeight: 700,
    },
  }));

  const { city, items, section, sectionSlug, Bgcolor, description } = props;

  const classes = useStyles();

  return (
    <Container>
      <Box mt={5}>
        <Box px={7}>
          <Box className={classes.title}>{toPascalCase(section)}</Box>{' '}
        </Box>
        <Box>
          <ItemSection
            items={items}
            city={city}
            section={section}
            sectionSlug={sectionSlug}
            Bgcolor={Bgcolor}
            coloums={3}
            isArchive={true}
            isSlider={false}
            noOfItems={100}
          />
        </Box>
      </Box>
    </Container>
  );
};

export default ArchivePage;
