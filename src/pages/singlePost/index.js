/* eslint-disable react/prop-types */
import { Avatar, Box, Container, Grid, makeStyles } from '@material-ui/core';
import TwitterIcon from '@material-ui/icons/Twitter';
import { motion } from 'framer-motion';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { SOCIAL_LINKS } from '../../../src/util/constants';
import BreadCrumbs from '../../components/common/breadCrumbs';
import useMediaQuery from '@material-ui/core/useMediaQuery';
const useStyles = makeStyles((theme) => ({
  exprecet: {
    color: 'grey',
    overflow: 'hidden',
    texOverflow: 'ellipsis',
    display: '-webkit-box',
    '-webkit-line-clamp': 3,
    '-webkit-box-orient': 'vertical',
  },
}));

const SinglePost = (props) => {
  const { item, highLights, city, sectionSlug, fstories } = props;
  const classes = useStyles();
  const router = useRouter();
  const matches = useMediaQuery('(min-width:600px)');

  return (
    <Container disableGutters>
      <Box mt={5}>
        <Box>
          <BreadCrumbs />
        </Box>
        <Box mt={1} ml={1}>
          <Box
            fontWeight={800}
            fontFamily="Roboto"
            fontSize={matches ? '3rem' : '1.5rem'}
          >
            {item.title}
          </Box>
          <Box fontSize="1rem" fontFamily="Roboto" mt={2} pr={matches ? 20 : 2}>
            <Box className={classes.exprecet}>
              {item?.excerpt?.replace(/(<([^>]+)>)/gi, '')}
            </Box>
          </Box>
        </Box>
        <Grid container display="flex">
          {matches ? (
            <Grid item md={3}>
              {HighLights(fstories, city, sectionSlug)}
            </Grid>
          ) : (
            <div />
          )}

          <Grid item xs={12} md={6}>
            <Box>
              <Box>
                <Box display="flex" flexDirection="column" px={matches ? 5 : 1} py={5}>
                  {/* AutorHead */}
                  <Box
                    display="flex"
                    flexWrap="wrap"
                    justifyContent="space-between"
                    style={{ marginBottom: '1rem' }}
                  >
                    <Box display="flex">
                      <Avatar style={{ height: '3.3rem', width: '3.3rem' }}>
                        {typeof item?.authorName[0] === 'undefined'
                          ? 'Nearbynews WebDesk'
                          : item.authorName[0]}
                      </Avatar>
                      <Box display="flex" flexDirection="column" ml={2}>
                        <Box
                          style={{
                            color: '#989898',
                            fontSize: '12px',
                            fontWeight: 700,
                          }}
                        >
                          {`${item.authorName}  `}
                          <a
                            rel="noreferrer"
                            href="https://twitter.com/intent/follow?screen_name=nearby_news"
                            target="_blank"
                          >
                            <TwitterIcon style={{ fontSize: '16px', color: '#00acee' }} />
                          </a>
                        </Box>
                        <Box
                          style={{
                            color: '#595959',
                            fontSize: '12px',
                            fontWeight: 400,
                          }}
                        >
                          {city}
                        </Box>
                        <Box
                          style={{
                            color: '#595959',
                            fontSize: '12px',
                            fontWeight: 400,
                          }}
                        >
                          {new Date(item.date).toDateString()}
                        </Box>
                      </Box>
                    </Box>

                    <Box
                      display="flex"
                      flexDirection="column"
                      style={{ marginTop: '-3px' }}
                    >
                      <Box
                        style={{
                          color: '#989898',
                          fontSize: '12px',
                          fontWeight: 700,
                        }}
                      >
                        {'share via : '}
                      </Box>
                      <Box
                        display="flex"
                        style={{
                          marginTop: '3px',
                        }}
                      >
                        <a
                          rel="noreferrer"
                          href={`https://www.facebook.com/sharer/sharer.php?u=${encodeURI(
                            `nearbynews.in${router.asPath}`,
                          )}`}
                          target="_blank"
                        >
                          <Avatar
                            src="/socialIcons/facebook.svg"
                            style={{
                              height: '2rem',
                              width: '2rem',
                              padding: '0.1rem',
                            }}
                          />
                        </a>
                        <a
                          rel="noreferrer"
                          href={`https://web.whatsapp.com/send?text=${encodeURI(
                            `nearbynews.in${router.asPath}`,
                          )}`}
                          target="_blank"
                        >
                          <Avatar
                            src="/socialIcons/whatsapp.svg"
                            style={{
                              height: '2rem',
                              width: '2rem',
                              color: 'white',
                              marginLeft: '5px',
                            }}
                          />
                        </a>
                        <a
                          rel="noreferrer"
                          href={`https://twitter.com/intent/tweet/?text=&url=${encodeURI(
                            `nearbynews.in${router.asPath}`,
                          )}`}
                          target="_blank"
                        >
                          <Avatar
                            src="/socialIcons/twitter.svg"
                            style={{
                              height: '2rem',
                              width: '2rem',
                              marginLeft: '5px',
                            }}
                          />
                        </a>

                        {/* <Box
                            onClick={() => window.print()}
                            style={{ cursor: 'pointer' }}
                          >
                            <Avatar
                              src="https://www.flaticon.com/svg/static/icons/svg/839/839184.svg"
                              style={{ height: '2rem', width: '2rem' }}
                            />
                          </Box> */}
                      </Box>
                    </Box>
                  </Box>
                  {/* featuredImage */}
                  <Image
                    src={
                      item.imageUrl ||
                      'https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png'
                    }
                    alt={item.title}
                    layout="responsive"
                    width={400}
                    height={300}
                    quality={100}
                  />

                  {/* ImageCaption */}
                  {/* <Paper square elevation={0} style={{ padding: '0.2rem 1rem' }}>
                    {item.Description || 'Image Caption Goes here'}
                  </Paper> */}
                  <Box my={matches ? 5 : 2}>
                    <div dangerouslySetInnerHTML={{ __html: item.content }} />
                  </Box>

                  <Box
                    alignItems="right"
                    display="flex"
                    style={{ paddingLeft: matches ? '19em' : '1rem' }}
                  >
                    <Box display="flex" alignItems="centre">
                      <p style={{ fontSize: '1rem' }}>FOLLOW US</p>
                      <Box
                        display="flex"
                        style={{ paddingTop: '1em', paddingLeft: '1em' }}
                      >
                        <a
                          href={SOCIAL_LINKS.FACEBOOK}
                          style={{ paddingLeft: '1em' }}
                          target="_blank"
                          rel="noreferrer"
                        >
                          <Avatar
                            style={{ width: '1.5rem', height: '1.5rem' }}
                            variant="square"
                            src="https://static.wixstatic.com/media/0fdef751204647a3bbd7eaa2827ed4f9.png"
                          />
                        </a>
                        <a
                          href={SOCIAL_LINKS.TWITTER}
                          style={{ paddingLeft: '1em' }}
                          target="_blank"
                          rel="noreferrer"
                        >
                          <Avatar
                            style={{ width: '1.5rem', height: '1.5rem' }}
                            variant="square"
                            src="https://static.wixstatic.com/media/c7d035ba85f6486680c2facedecdcf4d.png"
                          />
                        </a>
                        <a
                          href={SOCIAL_LINKS.LINKEDIN}
                          style={{ paddingLeft: '1em' }}
                          target="_blank"
                          rel="noreferrer"
                        >
                          <Avatar
                            style={{ width: '1.5rem', height: '1.5rem' }}
                            variant="square"
                            src="https://static.wixstatic.com/media/6ea5b4a88f0b4f91945b40499aa0af00.png"
                          />
                        </a>
                        <a
                          href={SOCIAL_LINKS.INSTAGRAM}
                          style={{ paddingLeft: '1em' }}
                          target="_blank"
                          rel="noreferrer"
                        >
                          <Avatar
                            style={{ width: '1.5rem', height: '1.5rem' }}
                            variant="square"
                            src="https://static.wixstatic.com/media/01c3aff52f2a4dffa526d7a9843d46ea.png"
                          />
                        </a>
                      </Box>
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Grid>
          <Grid item md={2}>
            {/* Advertisements Section */}
            {matches ? (
              <Box mt={4}>
                <Image
                  src="https://res.cloudinary.com/nearbynews/image/upload/v1608138740/bxk94puz0_xdahah.png"
                  alt={item.title}
                  layout="fixed"
                  width="300px"
                  height="250px"
                  quality={100}
                  loading="eager"
                />
                <Image
                  src="https://res.cloudinary.com/nearbynews/image/upload/v1608138769/90401b20a441effcd1a56dba03a6c9a6_hm3dg3.gif"
                  alt={item.title}
                  layout="fixed"
                  width="300px"
                  height="600px"
                  quality={100}
                  loading="eager"
                />
              </Box>
            ) : (
              <div />
            )}
          </Grid>
        </Grid>
      </Box>
      <Box my={1} mx={3}>
        {matches ? <div /> : HighLights(fstories, city, sectionSlug)}
      </Box>
      <Box my={1} mx={3}>
        {HighLightsH(highLights, city)}
      </Box>
    </Container>
  );
};

export default SinglePost;

const HighLights = (fstories, city) => (
  <Box my={3} mr={3} textAlign="center">
    <Box textAlign="left" fontSize="1.1rem" fontWeight="700" my={3}>
      Suggested Articles
    </Box>
    {fstories?.map((highLight) => (
      <motion.div
        whileHover={{ scale: 1.2 }}
        whileTap={{ scale: 0.8, borderRadius: '100%' }}
        animate={{ scale: 1 }}
        transformTemplate={(props, transform) =>
          // Disable GPU acceleration to prevent blurry text
          transform.replace(' translateZ(0)', '')
        }
        key={highLight.slug}
      >
        <Box my={2} display="flex" key={highLight.slug} textAlign="left">
          <Box display="flex">
            <Link
              as={`/${city}/stories/${highLight.slug}`}
              href="/[city]/stories/[slug]"
              prefetch={false}
            >
              <Box fontWeight={700} display="flex" style={{ cursor: 'pointer' }}>
                <Image
                  src={
                    highLight.imageUrl ||
                    'https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png'
                  }
                  alt={highLight.title}
                  width={100}
                  height={66}
                />

                <Box
                  ml={1}
                  fontSize="0.8rem"
                  display="flex"
                  alignItems="center"
                  maxWidth="70%"
                  style={{ height: '100%' }}
                >
                  {highLight.title}
                </Box>
              </Box>
            </Link>
          </Box>
          {/* </li> */}
        </Box>
      </motion.div>
    ))}
  </Box>
);

const HighLightsH = (highLights, city) => (
  <Box textAlign="left">
    <Box textAlign="left" fontSize="1.1rem" fontWeight="700" my={3}>
      Related Articles
    </Box>
    <Grid container spacing={1}>
      {highLights?.map((highLight) => (
        <Grid item md={3} key={highLight.slug}>
          <motion.div
            whileHover={{ scale: 1.2 }}
            whileTap={{ scale: 0.8, borderRadius: '100%' }}
            animate={{ scale: 1 }}
            transformTemplate={(props, transform) =>
              // Disable GPU acceleration to prevent blurry text
              transform.replace(' translateZ(0)', '')
            }
          >
            <Box my={1} display="flex" key={highLight.slug} textAlign="left">
              <Box display="flex">
                <Link
                  as={`/${city}/stories/${highLight.slug}`}
                  href="/[city]/stories/[slug]"
                  prefetch={false}
                >
                  <Box fontWeight={700} display="flex" style={{ cursor: 'pointer' }}>
                    <Image
                      src={
                        highLight.imageUrl ||
                        'https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png'
                      }
                      alt={highLight.title}
                      width={100}
                      height={70}
                    />
                    <Box ml={1} fontSize="0.8rem">
                      {highLight.title}
                    </Box>
                  </Box>
                </Link>
              </Box>
              {/* </li> */}
            </Box>
          </motion.div>
        </Grid>
      ))}
    </Grid>
  </Box>
);
