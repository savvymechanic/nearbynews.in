/* eslint-disable react/prop-types */
import { Box, Container, Paper, Typography, Grid } from '@material-ui/core';
import React from 'react';

import { useRouter } from 'next/router';
import { Twitter, Facebook, Linkedin, Mail, Whatsapp } from 'react-social-sharing';

const RenderSingleNewsLetter = (props) => {
  const { item, city } = props;
  const router = useRouter();
  return (
    <Container disableGutters>
      <Box mt={5}>
        <Grid container display="flex">
          <Grid item md={2}></Grid>
          <Grid item md={8}>
            <Box mt={4} mx={2}>
              <Paper elevation={1}>
                <Box display="flex" flexDirection="column" px={2} py={5}>
                  <Box my={5}>
                    <div dangerouslySetInnerHTML={{ __html: item.content }} />
                  </Box>
                  <Box>
                    <Whatsapp link={`nearbynews.in/${router.asPath}`} />
                    <Twitter link={`nearbynews.in/${router.asPath}`} />
                    <Facebook link={`nearbynews.in/${router.asPath}`} />
                    <Linkedin link={`nearbynews.in/${router.asPath}`} />
                    <Mail link={`nearbynews.in/${router.asPath}`} />
                  </Box>
                </Box>
              </Paper>
            </Box>
          </Grid>
          <Grid item md={2}></Grid>
        </Grid>
      </Box>
    </Container>
  );
};

export default RenderSingleNewsLetter;
