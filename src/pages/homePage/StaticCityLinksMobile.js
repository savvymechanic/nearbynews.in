import { Box, Paper } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import React, { useEffect, useState } from 'react';
export const StaticCityLinksMobile = (props) => {
  return (
    <Box
      display="flex"
      flexWrap="wrap"
      //   flexDirection={width > 992 ? 'row' : 'column'}
      justifyContent="space-evenly"
      alignItems="center"
      py={3}
    >
      <Paper
        elevation={0}
        style={{
          borderRadius: '2rem',
          boxShadow:
            props.selectedCity === 'hyderabad' ? '0px 5px 20px rgba(0,0,0,0.1)' : '',
          border: props.selectedCity === 'hyderabad' ? '2px solid rgb(241, 161, 4)' : '',
        }}
      >
        <Box
          m={1}
          mt={0}
          display="flex"
          flexDirection="column"
          alignItems="center"
          onClick={() => props.handleCityChange('hyderabad')}
          style={{
            cursor: 'pointer',
          }}
        >
          <Avatar
            style={{ width: '2.5rem', height: '2.5rem' }}
            variant="square"
            src="https://res.cloudinary.com/nearbynews/image/upload/v1606665674/noun_charminar_india_2165509_2_kg6ygu.svg"
          />
          <Box fontWeight="700" mt={1}>
            Hyderabad
          </Box>
        </Box>
      </Paper>

      <Paper
        elevation={0}
        style={{
          boxShadow:
            props.selectedCity === 'mumbai' ? '0px 5px 20px rgba(0,0,0,0.1)' : '',
          borderRadius: '2rem',
          border: props.selectedCity === 'mumbai' ? '2px solid rgb(241, 161, 4)' : '',
          background: 'transparent',
        }}
      >
        <Box
          m={1}
          display="flex"
          flexDirection="column"
          alignItems="center"
          onClick={() => props.handleCityChange('mumbai')}
          style={{
            cursor: 'pointer',
          }}
        >
          <Avatar
            style={{ width: '2rem', height: '2rem' }}
            variant="square"
            src="https://www.flaticon.com/svg/static/icons/svg/1283/1283983.svg"
          />
          <Box fontWeight="700" mt={1}>
            Mumbai
          </Box>
        </Box>
      </Paper>

      <Paper
        elevation={0}
        style={{
          borderRadius: '2rem',
          boxShadow: props.selectedCity === 'delhi' ? '0px 5px 20px rgba(0,0,0,0.1)' : '',
          border: props.selectedCity === 'delhi' ? '2px solid rgb(241, 161, 4)' : '',
          background: 'transparent',
        }}
      >
        <Box
          m={1}
          display="flex"
          flexDirection="column"
          alignItems="center"
          onClick={() => props.handleCityChange('delhi')}
          style={{
            cursor: 'pointer',
          }}
        >
          <Avatar
            style={{ width: '1.75rem', height: '2rem' }}
            variant="square"
            src="https://www.flaticon.com/svg/static/icons/svg/3174/3174792.svg"
          />
          <Box fontWeight="700" mt={1}>
            Delhi
          </Box>
        </Box>
      </Paper>

      <Paper
        elevation={0}
        style={{
          borderRadius: '2rem',
          boxShadow:
            props.selectedCity === 'bangalore' ? '0px 5px 20px rgba(0,0,0,0.1)' : '',
          border: props.selectedCity === 'bangalore' ? '2px solid rgb(241, 161, 4)' : '',
          background: 'transparent',
        }}
      >
        <Box
          m={1}
          display="flex"
          flexDirection="column"
          alignItems="center"
          onClick={() => props.handleCityChange('bangalore')}
          style={{
            cursor: 'pointer',
          }}
        >
          <Avatar
            style={{ width: '3rem', height: '2rem' }}
            variant="square"
            src="https://res.cloudinary.com/nearbynews/image/upload/v1606663067/vidhana_soudha_dfbjt0.svg"
          />
          <Box fontWeight="700" mt={1}>
            Bangalore
          </Box>
        </Box>
      </Paper>

      <Paper
        elevation={0}
        style={{
          borderRadius: '2rem',
          boxShadow:
            props.selectedCity === 'chennai' ? '0px 5px 20px rgba(0,0,0,0.1)' : '',
          border: props.selectedCity === 'chennai' ? '2px solid rgb(241, 161, 4)' : '',
          background: 'transparent',
        }}
      >
        <Box
          m={1}
          display="flex"
          flexDirection="column"
          alignItems="center"
          onClick={() => props.handleCityChange('chennai')}
          style={{
            cursor: 'pointer',
          }}
        >
          <Avatar
            style={{ width: '2rem', height: '2rem' }}
            variant="square"
            src="https://www.flaticon.com/svg/static/icons/svg/182/182249.svg"
          />
          <Box fontWeight="700" mt={1}>
            Chennai
          </Box>
        </Box>
      </Paper>
    </Box>
  );
};
