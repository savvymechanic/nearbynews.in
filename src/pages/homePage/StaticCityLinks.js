import { Box, Paper } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import React, { useEffect, useState } from 'react';
export const StaticCityLinks = (props) => {
  const [width, setWidth] = useState(0);

  useEffect(() => {
    setWidth(window.innerWidth);
  }, []);
  return (
    <Box
      display="flex"
    //   flexDirection={width > 992 ? 'row' : 'column'}
      justifyContent="center"
      alignItems="center"
      pt={3}
    >
     
      <Paper
        elevation={props.selectedCity === 'hyderabad' ? 8 : 0}
        style={{
          borderRadius: '2rem',
          border: props.selectedCity === 'hyderabad' ? '2px solid rgb(241, 161, 4)' : '',
          minWidth: '10rem',
        }}
      >
        <Box
          m={2}
          mt={0}
          display="flex"
          flexDirection="column"
          alignItems="center"
          onClick={() => props.handleCityChange('hyderabad')}
          style={{
            cursor: 'pointer',
          }}
        >
          <Avatar
            style={{ width: '5rem', height: '5rem' }}
            variant="square"
            src="https://res.cloudinary.com/nearbynews/image/upload/v1606665674/noun_charminar_india_2165509_2_kg6ygu.svg" />
          <Box fontWeight="700" mt={1}>
            Hyderabad
          </Box>
        </Box>
      </Paper>
      
     
      <Paper
        elevation={props.selectedCity === 'mumbai' ? 8 : 0}
        style={{
          borderRadius: '2rem',
          border: props.selectedCity === 'mumbai' ? '2px solid rgb(241, 161, 4)' : '',
          background: 'transparent',
          minWidth: '10rem',
        }}
      >
        <Box
          m={3}
          display="flex"
          flexDirection="column"
          alignItems="center"
          onClick={() => props.handleCityChange('mumbai')}
          style={{
            cursor: 'pointer',
          }}
        >
          <Avatar
            style={{ width: '4rem', height: '4rem' }}
            variant="square"
            src="https://www.flaticon.com/svg/static/icons/svg/1283/1283983.svg" />
          <Box fontWeight="700" mt={1}>
            Mumbai
          </Box>
        </Box>
      </Paper>

    
      <Paper
        elevation={props.selectedCity === 'delhi' ? 8 : 0}
        style={{
          borderRadius: '2rem',

          border: props.selectedCity === 'delhi' ? '2px solid rgb(241, 161, 4)' : '',
          background: 'transparent',
          minWidth: '10rem',
        }}
      >
        <Box
          m={3}
          display="flex"
          flexDirection="column"
          alignItems="center"
          onClick={() => props.handleCityChange('delhi')}
          style={{
            cursor: 'pointer',
          }}
        >
          <Avatar
            style={{ width: '3.5rem', height: '4rem' }}
            variant="square"
            src="https://www.flaticon.com/svg/static/icons/svg/3174/3174792.svg" />
          <Box fontWeight="700" mt={1}>
            Delhi
          </Box>
        </Box>
      </Paper>
    
    
      <Paper
        elevation={props.selectedCity === 'bangalore' ? 8 : 0}
        style={{
          borderRadius: '2rem',

          border: props.selectedCity === 'bangalore' ? '2px solid rgb(241, 161, 4)' : '',
          background: 'transparent',
          minWidth: '10rem',
        }}
      >
        <Box
          m={3}
          display="flex"
          flexDirection="column"
          alignItems="center"
          onClick={() => props.handleCityChange('bangalore')}
          style={{
            cursor: 'pointer',
          }}
        >
          <Avatar
            style={{ width: '6rem', height: '4rem' }}
            variant="square"
            src="https://res.cloudinary.com/nearbynews/image/upload/v1606663067/vidhana_soudha_dfbjt0.svg" />
          <Box fontWeight="700" mt={1}>
            Bangalore
          </Box>
        </Box>
      </Paper>
     
     
      <Paper
        elevation={props.selectedCity === 'chennai' ? 8 : 0}
        style={{
          borderRadius: '2rem',
          border: props.selectedCity === 'chennai' ? '2px solid rgb(241, 161, 4)' : '',
          background: 'transparent',
          minWidth: '10rem',
        }}
      >
        <Box
          m={3}
          display="flex"
          flexDirection="column"
          alignItems="center"
          onClick={() => props.handleCityChange('chennai')}
          style={{
            cursor: 'pointer',
          }}
        >
          <Avatar
            style={{ width: '4rem', height: '4rem' }}
            variant="square"
            src="https://www.flaticon.com/svg/static/icons/svg/182/182249.svg" />
          <Box fontWeight="700" mt={1}>
            Chennai
          </Box>
        </Box>
      </Paper>
   
   
    </Box>
  );
};
