/* eslint-disable react/prop-types */
import { Box, Container } from '@material-ui/core';
import React from 'react';
import ItemSection from '../../components/common/itemSection';
import AboutUSSection from '../../components/sections/aboutUsSection';
import LandingPageHead from '../../components/sections/landingPageHead';
import Testimonials from '../../components/sections/testimonials';
import { StaticCityLinks } from './StaticCityLinks';
import { StaticCityLinksMobile } from './StaticCityLinksMobile';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import AboutUsSectionMobile from '../../components/sections/aboutUsSectionMobile';
import LandingPageHeadMobile from '../../components/sections/landingPageHeadMobile';
import ItemSectionTabs from '../../components/common/itemSectionTabs';

const RenderHomePage = (props) => {
  const isMin600px = useMediaQuery('(min-width:600px)');
  const { HomePageData: HomePageData } = props;
  const [selectedCity, setCurrentCity] = React.useState(HomePageData[0]);

  const handleCityChange = (SelectedCity) => {
    var SelectedCityIndex = 0;
    HomePageData.forEach((city, index) => {
      if (city.city === SelectedCity) {
        SelectedCityIndex = index;
      }
    });
    setCurrentCity(HomePageData[SelectedCityIndex]);
  };

  return (
    <Container disableGutters>
      {isMin600px ? <LandingPageHead /> : <LandingPageHeadMobile />}

      {isMin600px ? (
        <StaticCityLinks
          selectedCity={selectedCity.city}
          handleCityChange={handleCityChange}
        />
      ) : (
        <StaticCityLinksMobile
          selectedCity={selectedCity.city}
          handleCityChange={handleCityChange}
        />
      )}
      {isMin600px ? (
        <>
          <Box py={1} px={2}>
            <section id="newsletters">
              <ItemSection
                items={selectedCity.newsletters}
                city={selectedCity.city}
                section="Newsletters"
                sectionSlug="newsletters"
                Bgcolor="#16418f"
                as={`/${selectedCity.city}/newsletters`}
                href="/[city]/newsletters"
                coloums={3}
                noOfItems={10}
                isSlider={true}
                autoplay={false}
              />
            </section>
          </Box>

          <Box py={2} px={2}>
            <section id="stories">
              <ItemSection
                items={selectedCity.stories}
                city={selectedCity.city}
                section="Stories"
                sectionSlug="stories"
                Bgcolor="#f9c100"
                as={`/${selectedCity.city}/stories`}
                href="/[city]/stories"
                coloums={3}
                noOfItems={10}
                isSlider
                sliderSpeed={1000}
                autoplay={true}
              />
            </section>
          </Box>
          <Box py={2} px={2}>
            <section id="cityspeaks">
              <ItemSection
                items={selectedCity.citySpeaks}
                city={selectedCity.city}
                section="City Speaks"
                sectionSlug="cityspeaks"
                Bgcolor="#16418f"
                as={`/${selectedCity.city}/cityspeaks`}
                href="/[city]/cityspeaks"
                coloums={3}
                noOfItems={10}
                isSlider
                sliderSpeed={1000}
                autoplay={true}
              />
            </section>
          </Box>
        </>
      ) : (
        <Box>
          <ItemSectionTabs
            newsletters={selectedCity.newsletters}
            stories={selectedCity.stories}
            city={selectedCity.city}
          />
        </Box>
      )}

      <Box py={1} px={4}>
        {isMin600px ? <AboutUSSection /> : <AboutUsSectionMobile />}
      </Box>
      <Testimonials />
    </Container>
  );
};

export default RenderHomePage;

const Cities = [
  {
    city: 'Hyderabad',
    src:
      'https://res.cloudinary.com/nearbynews/image/upload/v1606665674/noun_charminar_india_2165509_2_kg6ygu.svg',
  },
  {
    city: 'Mumbai',
    src: 'https://www.flaticon.com/svg/static/icons/svg/1283/1283983.svg',
  },

  {
    city: 'Delhi',

    src: 'https://www.flaticon.com/svg/static/icons/svg/3174/3174792.svg',
  },
  {
    city: 'bangalore',
    src:
      'https://res.cloudinary.com/nearbynews/image/upload/v1606663067/vidhana_soudha_dfbjt0.svg',
  },
  {
    city: 'Chennai',
    src: 'https://www.flaticon.com/svg/static/icons/svg/182/182249.svg',
  },
];
