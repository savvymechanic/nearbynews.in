export const IP_URL = 'https://ipinfo.io/json?token=979e220156bdfb';
export const CORS_PROXY = 'https://cors-anywhere.herokuapp.com/';
export const WEATHER_API_KEY = 'f03b21af3bfe7b71292d314d12077aa3'; //Please store it in env variables for the sake of cyber security
export const WEATHER_API_URL = 'http://api.openweathermap.org/data/2.5/weather';

export const SOCIAL_LINKS = {
  FACEBOOK: 'https://www.facebook.com/nearby-news-104492848070149/',
  INSTAGRAM: 'https://www.instagram.com/nearby_news/',
  TWITTER: 'https://twitter.com/nearby_news',
  LINKEDIN: 'https://www.linkedin.com/company/nearby-news/',
};

export const SOCIAL_ICON_LINKS = {
  FACEBOOK:
    'https://res.cloudinary.com/nearbynews/image/upload/v1608018968/Dont%20delete/facebook_ataezm.svg',
  INSTAGRAM:
    'https://res.cloudinary.com/nearbynews/image/upload/v1608019078/Dont%20delete/1384031_s6hvea.svg',
  TWITTER:
    'https://res.cloudinary.com/nearbynews/image/upload/v1608019027/Dont%20delete/733635_hak2ow.svg',
  LINKEDIN:
    'https://res.cloudinary.com/nearbynews/image/upload/v1608019143/Dont%20delete/2111532_xa0p1i.svg',
};
