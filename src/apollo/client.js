import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client';
import { createPersistedQueryLink } from 'apollo-link-persisted-queries';

const defaultOptions = {
  watchQuery: {
    fetchPolicy: 'cache-and-network',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  },
};

/**
 * Instantiate required constructor fields
 */
const cache = new InMemoryCache({
  resultCaching: false,
});

const link = createPersistedQueryLink({ useGETForHashedQueries: true }).concat(
  createHttpLink({ uri: 'https://nearbynews.headlesswp.tech/graphql' }),
);

const client = new ApolloClient({
  cache,
  link,
  defaultOptions: defaultOptions,
  ssrMode: true,
});

export default client;
