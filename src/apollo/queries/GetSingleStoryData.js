const { gql } = require('@apollo/client');

export const GetSingleStoryData = gql`
  query GetSingleStoryData($city: String!, $slug: String!, $count: Int!) {
    story: stories(where: { name: $slug }) {
      edges {
        node {
          slug
          title
          excerpt
          author {
            node {
              avatar {
                url
              }
              name
            }
          }
          content(format: RENDERED)
          dateGmt
          featuredImage {
            node {
              sourceUrl
              mediaItemUrl
              caption
            }
          }
          terms {
            edges {
              node {
                ... on Tag {
                  id
                  name
                }
              }
            }
          }
        }
      }
    }
    recentStories: stories(
      where: {
        taxQuery: {
          taxArray: { taxonomy: CITY, terms: [$city], field: SLUG, includeChildren: true }
        }
        orderby: { field: DATE, order: DESC }
      }
      first: $count
    ) {
      edges {
        node {
          dateGmt
          date
          featuredImage {
            node {
              mediaItemUrl
              sourceUrl
            }
          }
          title(format: RENDERED)
          slug
          author {
            node {
              name
            }
          }
          categories(first: 1) {
            edges {
              node {
                name
              }
            }
          }
        }
      }
    }

    fStories: stories(
      where: {
        taxQuery: {
          taxArray: [
            { taxonomy: CITY, terms: [$city], field: SLUG, includeChildren: true }
            { taxonomy: CATEGORY, terms: ["featured"], field: SLUG }
          ]
        }
        orderby: { field: DATE, order: DESC }
      }
      first: $count
    ) {
      edges {
        node {
          dateGmt
          date
          featuredImage {
            node {
              mediaItemUrl
              sourceUrl
            }
          }
          title(format: RENDERED)
          slug
          author {
            node {
              name
            }
          }
          categories(first: 1) {
            edges {
              node {
                name
              }
            }
          }
        }
      }
    }
  }
`;
