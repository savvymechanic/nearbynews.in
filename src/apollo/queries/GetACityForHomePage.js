const { gql } = require('@apollo/client');

export const GetACityForHomePage = gql`
  query GetACityForHomePage($city: String!, $count: Int!) {
    newsletters(
      where: {
        taxQuery: {
          taxArray: { taxonomy: CITY, terms: [$city], field: SLUG, includeChildren: true }
        }
        orderby: { field: DATE, order: DESC }
      }
      first: $count
    ) {
      edges {
        node {
          dateGmt
          date
          featuredImage {
            node {
              mediaItemUrl
              sourceUrl
            }
          }
          title(format: RENDERED)
          slug
          author {
            node {
              name
            }
          }
        }
      }
    }

    stories(
      where: {
        taxQuery: {
          taxArray: { taxonomy: CITY, terms: [$city], field: SLUG, includeChildren: true }
        }
        orderby: { field: DATE, order: DESC }
      }
      first: $count
    ) {
      edges {
        node {
          dateGmt
          date
          featuredImage {
            node {
              mediaItemUrl
              sourceUrl
            }
          }
          title(format: RENDERED)
          slug
          author {
            node {
              name
            }
          }
          categories(first: 1) {
            edges {
              node {
                name
              }
            }
          }
        }
      }
    }

    cityspeaks(
      where: {
        taxQuery: {
          taxArray: { taxonomy: CITY, terms: [$city], field: SLUG, includeChildren: true }
        }
        orderby: { field: DATE, order: DESC }
      }
      first: $count
    ) {
      edges {
        node {
          dateGmt
          date
          featuredImage {
            node {
              mediaItemUrl
              sourceUrl
            }
          }
          title(format: RENDERED)
          slug
          author {
            node {
              name
            }
          }
        }
      }
    }
  }
`;
