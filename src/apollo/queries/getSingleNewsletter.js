const { gql } = require('@apollo/client');

export const GetSingleNewsletter = gql`
  query GetSingleNewsletter($slug: String!) {
    newsletters(where: { name: $slug }) {
      edges {
        node {
          slug
          excerpt
          title
          author {
            node {
              avatar {
                url
              }
              name
            }
          }
          content(format: RENDERED)
          dateGmt
          featuredImage {
            node {
              sourceUrl
              mediaItemUrl
            }
          }
          terms {
            edges {
              node {
                ... on Tag {
                  id
                  name
                }
              }
            }
          }
        }
      }
    }
  }
`;
