const { gql } = require('@apollo/client');

export const GetSingleStory = gql`
  query GetSingleStory($slug: String!) {
    stories(where: { name: $slug }) {
      edges {
        node {
          slug
          title
          excerpt
          author {
            node {
              avatar {
                url
              }
              name
            }
          }
          content(format: RENDERED)
          dateGmt
          featuredImage {
            node {
              sourceUrl
              mediaItemUrl
              caption
            }
          }
          terms {
            edges {
              node {
                ... on Tag {
                  id
                  name
                }
              }
            }
          }
        }
      }
    }
  }
`;
