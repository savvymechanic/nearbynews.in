const { gql } = require('@apollo/client');

export const GetStoriesOfCity = gql`
  query GetStoriesOfCity($city: String!, $count: Int!) {
    stories(
      where: {
        taxQuery: {
          taxArray: { taxonomy: CITY, terms: [$city], field: SLUG, includeChildren: true }
        }
        orderby: { field: DATE, order: DESC }
      }
      first: $count
    ) {
      edges {
        node {
          dateGmt
          date
          featuredImage {
            node {
              mediaItemUrl
              sourceUrl
            }
          }
          title(format: RENDERED)
          slug
          author {
            node {
              name
            }
          }
          categories(first: 1) {
            edges {
              node {
                name
              }
            }
          }
        }
      }
    }
  }
`;
