const { gql } = require('@apollo/client');

export const GetSingleCitySpeak = gql`
  query GetSingleCitySpeak($slug: String!) {
    cityspeaks(where: { name: $slug }) {
      edges {
        node {
          slug
          excerpt
          title
          author {
            node {
              avatar {
                url
              }
              name
            }
          }
          content(format: RENDERED)
          dateGmt
          featuredImage {
            node {
              sourceUrl
              mediaItemUrl
            }
          }
        }
      }
    }
  }
`;
