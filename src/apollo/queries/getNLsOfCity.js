const { gql } = require('@apollo/client');

export const GetNLsOfCity = gql`
  query GetNLsOfCity($city: String!, $count: Int!) {
    newsletters(
      where: {
        taxQuery: {
          taxArray: { taxonomy: CITY, terms: [$city], field: SLUG, includeChildren: true }
        }
        orderby: { field: DATE, order: DESC }
      }
      first: $count
    ) {
      edges {
        node {
          dateGmt
          date
          featuredImage {
            node {
              mediaItemUrl
              sourceUrl
            }
          }
          title(format: RENDERED)
          slug
          author {
            node {
              name
            }
          }
        }
      }
    }
  }
`;
