const { gql } = require('@apollo/client');

export const GetCities = gql`
  query GetCities {
    cities: cities(first: 200) {
      edges {
        node {
          name
          slug
        }
      }
    }
  }
`;
