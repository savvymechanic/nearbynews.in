const { gql } = require('@apollo/client');

export const GetFeaturedStoriesOfCity = gql`
  query GetFeaturedStoriesOfCity($city: String!, $count: Int!) {
    stories(
      where: {
        taxQuery: {
          taxArray: [
            { taxonomy: CITY, terms: [$city], field: SLUG, includeChildren: true }
            { taxonomy: CATEGORY, terms: ["featured"], field: SLUG }
          ]
        }
        orderby: { field: DATE, order: DESC }
      }
      first: $count
    ) {
      edges {
        node {
          dateGmt
          date
          featuredImage {
            node {
              mediaItemUrl
              sourceUrl
            }
          }
          title(format: RENDERED)
          slug
          author {
            node {
              name
            }
          }
          categories(first: 1) {
            edges {
              node {
                name
              }
            }
          }
        }
      }
    }
  }
`;
