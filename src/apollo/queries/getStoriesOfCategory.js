const { gql } = require('@apollo/client');

export const GetStoriesOfCategory = gql`
  query GetStoriesOfCategory($city: String!, $category: String, $count: Int!) {
    stories(
      where: {
        taxQuery: {
          taxArray: {
            taxonomy: CITY
            terms: [$city]
            field: SLUG
            includeChildren: false
          }
        }
        orderby: { field: DATE, order: DESC }
        categoryName: $category
      }
      first: $count
    ) {
      edges {
        node {
          dateGmt
          date
          featuredImage {
            node {
              mediaItemUrl
              sourceUrl
            }
          }
          title(format: RENDERED)
          slug
          author {
            node {
              name
            }
          }
          categories(first: 1) {
            edges {
       
              node {
                name
              }
            }
          }
        }
      }
    }
  }
`;
