import React, { useEffect } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Box, Button, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { useForm } from 'react-hook-form';
import { SubsBoxDesktop } from './SubsBoxDesktop';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { SubsBoxMobile } from './SubsBoxMobile';

const client = require('@sendgrid/client');
var http = require('https');
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  textField: {
    [`& fieldset`]: {
      borderRadius: 0,
      border: '0px',
      // borderBottom: '1px solid black',
      // borderTop: '1px solid black',
    },
  },
}));

export default function MiniSubsBox(props) {
  const classes = useStyles();
  const matches = useMediaQuery('(min-width:600px)');

  const { register, handleSubmit } = useForm();
  const [open, setOpen] = React.useState(false);

  // let window = null;
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const onSubmit = (data) => {
    handleClickOpen();
    var options = {
      method: 'PUT',
      hostname: 'api.sendgrid.com',
      port: null,
      path: '/v3/marketing/contacts',
      headers: {
        authorization:
          'Bearer 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',
        'content-type': 'application/json',
      },
    };

    var req = http.request(options, function (res) {
      var chunks = [];

      res.on('data', function (chunk) {
        chunks.push(chunk);
      });

      res.on('end', function () {
        var body = Buffer.concat(chunks);
        console.log(body.toString());
      });
    });

    req.write(
      JSON.stringify({
        contacts: [
          {
            city: city,
            country: 'India',
            email: data.email,
            postal_code: data.pincode,
          },
        ],
      }),
    );
    req.end();
  };
  const [city, setCity] = React.useState('hyderabad');
  const handleChange = (event) => {
    setCity(event.target.value);
    console.log(event.target.value);
  };
  return (
    <Box mt={props.isFooter ? 0 : 3}>
      {matches ? (
        <SubsBoxDesktop
          handleSubmit={handleSubmit}
          onSubmit={onSubmit}
          city={city}
          handleChange={handleChange}
          register={register}
          classes={classes}
          props={props}
        />
      ) : (
        <SubsBoxMobile
          handleSubmit={handleSubmit}
          onSubmit={onSubmit}
          city={city}
          handleChange={handleChange}
          register={register}
          classes={classes}
          props={props}
        />
      )}
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {'Subscribed ! Thank you'}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            We don’t spam you!
          </DialogContentText>
          <DialogContentText id="alert-dialog-slide-description">
            We do not want to add to the clutter in the inbox! we get it!
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            close
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}
