import React from 'react';
import TextField from '@material-ui/core/TextField';
import { Box, Button, Select, MenuItem, FormControl } from '@material-ui/core';
export const SubsBoxDesktop = ({
  handleSubmit,
  onSubmit,
  city,
  handleChange,
  register,
  classes,
  props,
}) => {
  return (
    <form
      onSubmit={handleSubmit((data) => onSubmit(data))}
      style={{
        display: 'flex',
        border: '2px solid gray',
        borderRadius: '0.75rem',
        width: 'fit-content',
        overflow: 'hidden',
      }}
    >
      <FormControl variant="outlined" size="small">
        <Select
          value={city}
          onChange={handleChange}
          inputRef={register}
          className={classes.textField}
          style={{
            // borderLeft: '1px solid black',
            // borderRight: '1px solid #e0e0e0',
            border: 'none',
            width: '9rem',
            borderRight: '1px solid #e0e0e0',
          }}
        >
          <MenuItem value={'hyderabad'}>Hyderabad</MenuItem>
          <MenuItem value={'chennai'}>Chennai</MenuItem>
          <MenuItem value={'mumbai'}>Mumbai</MenuItem>
          <MenuItem value={'delhi'}>Delhi</MenuItem>
          <MenuItem value={'bangalore'}>Bangalore</MenuItem>
        </Select>
      </FormControl>

      <TextField
        className={classes.textField}
        placeholder="Pincode"
        variant="outlined"
        // required
        inputRef={register({
          pattern: /\(|\)|\d{6}/i,
        })}
        id="pincode"
        name="pincode"
        size="small"
        margin="none"
        style={{
          width: '7rem',
          //  border: '1px solid black',
          borderRight: '1px solid #e0e0e0',
        }}
      />

      <TextField
        className={classes.textField}
        // label="Email"
        placeholder="Email"
        type="email"
        variant="outlined"
        required
        inputRef={register}
        id="email"
        name="email"
        size="small"
      />

      <Box
        style={{
          backgroundColor: '#f1a104',
          display: 'flex',
          height: props.isFooter ? '43px' : '42px',
        }}
      >
        <Button
          type="submit"
          variant="contained"
          style={{
            // marginLeft: '1px',
            borderRadius: '0.75rem',
            // marginBottom: '-0.5px',
            // marginRight: '-1px',
            color: 'black',
            backgroundColor: '#f1a104',
          }}
          disableElevation={true}
          disableFocusRipple={true}
        >
          Subscribe
        </Button>
      </Box>
    </form>
  );
};
