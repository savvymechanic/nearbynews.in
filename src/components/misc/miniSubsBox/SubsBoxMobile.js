import React from 'react';
import TextField from '@material-ui/core/TextField';
import { Box, Button, Select, MenuItem, FormControl } from '@material-ui/core';
export const SubsBoxMobile = ({
  handleSubmit,
  onSubmit,
  city,
  handleChange,
  register,
  classes,
  props,
}) => {
  return (
    <form
      onSubmit={handleSubmit((data) => onSubmit(data))}
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        border: '2px solid gray',
        borderRadius: '0.3rem',
        width: 'fit-content',
        overflow: 'hidden',
        margin: '0px -5px 0px 0px',
      }}
    >
      <FormControl
        variant="outlined"
        size="small"
        style={{
          width: '50%',
          borderBottom: '1px solid #e0e0e0',
        }}
      >
        <Select
          value={city}
          onChange={handleChange}
          inputRef={register}
          className={classes.textField}
          style={{
            border: 'none',
            borderRight: '1px solid #e0e0e0',
          }}
        >
          <MenuItem value={'hyderabad'}>Hyderabad</MenuItem>
          <MenuItem value={'chennai'}>Chennai</MenuItem>
          <MenuItem value={'mumbai'}>Mumbai</MenuItem>
          <MenuItem value={'delhi'}>Delhi</MenuItem>
          <MenuItem value={'bangalore'}>Bangalore</MenuItem>
        </Select>
      </FormControl>

      <TextField
        className={classes.textField}
        placeholder="Pincode"
        variant="outlined"
        // required
        inputRef={register({
          pattern: /\(|\)|\d{6}/i,
        })}
        id="pincode"
        name="pincode"
        size="small"
        margin="none"
        style={{
          width: '50%',
          borderRight: '1px solid #e0e0e0',
          borderBottom: '1px solid #e0e0e0',
        }}
      />

      <TextField
        className={classes.textField}
        placeholder="Email"
        type="email"
        variant="outlined"
        required
        inputRef={register}
        id="email"
        name="email"
        size="small"
        style={{
          width: '100%',
        }}
      />

      <Box
        style={{
          backgroundColor: '#f1a104',
          display: 'flex',
          height: '43px',
          width: '100%',
        }}
      >
        <Button
          type="submit"
          variant="contained"
          style={{
            borderRadius: '0.3rem',
            color: 'black',
            backgroundColor: '#f1a104',
            width: '100%',
            height: '100%',
            margin: '0rem 0rem -1rem 0rem',
          }}
          disableElevation={true}
          disableFocusRipple={true}
        >
          Subscribe
        </Button>
      </Box>
    </form>
  );
};
