import React from 'react';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

const PincodeModal = (props) => (
  <Dialog
    open={props.open}
    onClose={props.handleClose}
    aria-labelledby="form-dialog-title"
  >
    <DialogTitle id="form-dialog-title">Pincode</DialogTitle>
    <DialogContent>
      <DialogContentText>
        Please enter your Pincode to see your nearbynews. Drop your email to receive them
        directly to your inbox.
      </DialogContentText>
      <TextField
        autoFocus
        id="pincode"
        label="Pincode"
        type="text"
        fullWidth
        required={true}
      />
      <TextField
        id="name"
        label="Email Address"
        type="email"
        fullWidth
        style={{
          margin: '1rem 0rem',
        }}
      />
    </DialogContent>
    <DialogActions>
      <Button onClick={props.handleClose} color="primary">
        open
      </Button>
      <Button onClick={props.handleClose} color="primary" variant="contained">
        Subscribe & open
      </Button>
    </DialogActions>
  </Dialog>
);

export default PincodeModal;
