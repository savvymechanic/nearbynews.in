import React from 'react';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Snackbar from '@material-ui/core/Snackbar';

const AlertMessage = (props) => {
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      open={props.alertopen}
      autoHideDuration={6000}
      onClose={props.handleAlertClose}
      message="Please Select your city"
      action={
        <React.Fragment>
          <Button color="secondary" size="small" onClick={props.handleClickOpen}>
            open
          </Button>
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={props.handleAlertClose}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        </React.Fragment>
      }
    />
  );
};

export default AlertMessage;
