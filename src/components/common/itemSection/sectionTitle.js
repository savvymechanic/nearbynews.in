/* eslint-disable react/prop-types */
import { Box, Container, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import Link from 'next/link';
import React from 'react';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import styles from './styles';
const useStyles = makeStyles(styles);

const SectionTitle = (props) => {
  const { items, section, as, href, isArchive } = props;
  const classes = useStyles();
  return (
    <Box>
      {items.length > 0 ? (
        <Box mt={0}>
          {/*   Section Title   */}
          {isArchive ? (
            <div />
          ) : (
            <Box className={classes.HeadingBox}>
              <Box className={classes.Heading}>
                <Box className={classes.SectionTitle}>{section}</Box>
              </Box>
              <span className={classes.HorizontalBar}></span>
              {isArchive ? (
                <div />
              ) : (
                <Link as={as} href={href} prefetch={false}>
                  <Box className={classes.ReadMore}>
                    <Box style={{ paddingRight: '5px' }}>
                      <span style={{ fontWeight: '600', fontSize: '16px' }}>
                        Read More
                      </span>
                    </Box>
                    <ArrowForwardIosIcon style={{ fontSize: 13 }} />
                  </Box>
                </Link>
              )}
            </Box>
          )}
        </Box>
      ) : (
        <div />
      )}
    </Box>
  );
};

export default SectionTitle;
