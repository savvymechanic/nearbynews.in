/* eslint-disable react/prop-types */
import { Box, Container, Grid, makeStyles } from '@material-ui/core';
import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import { uuidv4 } from '../../../../utils/functions/stringManipulation';
import HomeItemCard from '../itemCard';
import { SampleNextArrow, SamplePrevArrow } from '../sliderArrows';
import SectionTitle from './sectionTitle';
import styles from './styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Link from 'next/link';
import Image from 'next/image';

const useStyles = makeStyles(styles);
const ItemSection = (props) => {
  const {
    items,
    city,
    section,
    sectionSlug,
    as,
    href,
    coloums,
    isSlider,
    isArchive,
    noOfItems,
    sliderSpeed,
    autoplay,
  } = props;
  const classes = useStyles();
  const matches = useMediaQuery('(min-width:600px)');

  let settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: matches ? 4 : 1,
    slidesToScroll: 1,
    autoplay: autoplay,
    autoplaySpeed: sliderSpeed || 2000,
    arrows: matches ? true : false,
    nextArrow: matches ? <SampleNextArrow /> : <div />,
    prevArrow: matches ? <SamplePrevArrow /> : <div />,
  };
  return (
    <Box>
      <Container>
        {items?.length > 4 ? (
          <Box mt={3}>
            <SectionTitle
              items={items}
              section={section}
              as={as}
              href={href}
              isArchive={isArchive}
            />
            <style jsx global>{`
              .slick-list {
                height: 22rem;
              }
              .slick-slide {
                padding: 0.2rem;
              }
            `}</style>
            {isSlider ? (
              matches ? (
                <Slider {...settings}>
                  {items.map((item, i) => {
                    var tag;
                    if (sectionSlug != 'stories') {
                      tag = new Date(item.date).toDateString();
                    } else {
                      tag = item.category || 'General';
                    }
                    return (
                      <Box className={classes.ItemCard} key={uuidv4()}>
                        <HomeItemCard
                          section={section}
                          image={item.imageUrl}
                          tag={tag}
                          title={item.title}
                          city={city}
                          slug={item.slug}
                          sectionSlug={sectionSlug}
                          authorName={item.authorName}
                          isArchive={isArchive}
                          date={new Date(item.date).toDateString()}
                        />
                      </Box>
                    );
                  })}
                </Slider>
              ) : (
                SmallStories(items, city, sectionSlug)
              )
            ) : (
              <Grid
                container
                mt={2}
                className={classes.CardsContainer}
                spacing={2}
                direction="row"
              >
                {items?.slice(0, noOfItems).map((item, i) => {
                  var tag;
                  if (sectionSlug != 'stories') {
                    tag = new Date(item.date).toDateString();
                  } else {
                    tag = item.category || 'General';
                  }
                  return (
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={coloums}
                      className={classes.ItemCard}
                      key={i}
                    >
                      <HomeItemCard
                        section={section}
                        image={item.imageUrl}
                        tag={tag}
                        title={item.title}
                        city={city}
                        slug={item.slug}
                        sectionSlug={sectionSlug}
                        authorName={item.authorName}
                        isArchive={isArchive}
                        date={new Date(item.date).toDateString()}
                      />
                    </Grid>
                  );
                })}
              </Grid>
            )}

            {/*   Item Cards  */}
          </Box>
        ) : (
          <div />
        )}
      </Container>
    </Box>
  );
};

export default ItemSection;

const SmallStories = (items, city, section) => (
  <Box my={1} mr={3} textAlign="center">
    {items?.map((highLight) => (
      <Box my={2} display="flex" key={highLight.slug} textAlign="left">
        <Box display="flex">
          <Link
            as={`/${city}/${section}/${highLight.slug}`}
            href={`/[city]/${section}/[slug]`}
            prefetch={false}
          >
            <Box fontWeight={700} display="flex" style={{ cursor: 'pointer' }}>
              <Image
                src={
                  highLight.imageUrl ||
                  'https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png'
                }
                alt={highLight.title}
                width={100}
                height={66}
              />

              <Box
                ml={1}
                fontSize="0.8rem"
                display="flex"
                alignItems="center"
                maxWidth="70%"
                style={{ height: '100%' }}
              >
                {highLight.title}
              </Box>
            </Box>
          </Link>
        </Box>
        {/* </li> */}
      </Box>
    ))}
  </Box>
);
