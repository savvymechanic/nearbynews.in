/* eslint-disable react/prop-types */
import { Box, Container, Grid, makeStyles } from '@material-ui/core';
import React from 'react';

import { uuidv4 } from '../../../../utils/functions/stringManipulation';

import styles from './styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Link from 'next/link';
import Image from 'next/image';

const useStyles = makeStyles(styles);
const ItemSectionTabs = (props) => {
  const { stories, newsletters, city } = props;

  const [tab, setTab] = React.useState('newsletters');

  React.useEffect(() => {
    if (newsletters.length === 0) {
      setTab('stories');
    }
  }, [props]);
  return (
    <Box>
      <Container>
        <Box display="flex" flexDirection="column" alignItems="center">
          <Box
            display="flex"
            justifyContent="space-around"
            my={3}
            style={{
              borderRadius: '50px',
              background: '#fff',
              boxShadow: '0px 5px 20px rgba(0,0,0,0.1)',
              width: 'fit-content',
            }}
          >
            {newsletters.length > 0 && (
              <Box
                p={2}
                component="div"
                onClick={() => {
                  setTab('newsletters');
                }}
                style={{
                  borderRadius: '50px',
                  color: tab === 'newsletters' ? 'white' : 'grey',
                  background:
                    tab === 'newsletters'
                      ? 'linear-gradient(45deg, #05abe0 0%,#8200f4 100%)'
                      : 'white',
                  visibility: newsletters.length > 0 ? 'visible' : 'collapse',
                }}
              >
                Newsletters
              </Box>
            )}
            <Box
              onClick={() => {
                setTab('stories');
              }}
              p={2}
              style={{
                borderRadius: '50px',
                color: tab === 'stories' ? 'white' : 'grey',
                background:
                  tab === 'stories'
                    ? 'linear-gradient(45deg, #05abe0 0%,#8200f4 100%)'
                    : 'white',
                minWidth: '6.3rem',
                textAlign: 'center',
              }}
            >
              Stories
            </Box>
          </Box>
        </Box>
        <Box pl={3}>
          {tab === 'newsletters' ? (
            <SmallStories items={newsletters} city={city} section="newsletters" />
          ) : (
            <SmallStories items={stories} city={city} section="stories" />
          )}
        </Box>
      </Container>
    </Box>
  );
};

export default ItemSectionTabs;

const SmallStories = ({ items, city, section }) => (
  <Box my={1} mr={3} textAlign="center">
    {items?.map((highLight) => (
      <Box my={2} display="flex" key={highLight.slug} textAlign="left">
        <Box display="flex">
          <Link
            as={`/${city}/${section}/${highLight.slug}`}
            href={`/[city]/${section}/[slug]`}
            prefetch={false}
          >
            <Box fontWeight={700} display="flex" style={{ cursor: 'pointer' }}>
              <Image
                src={
                  highLight.imageUrl ||
                  'https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png'
                }
                alt={highLight.title}
                width={100}
                height={66}
              />

              <Box
                ml={1}
                fontSize="0.8rem"
                display="flex"
                alignItems="center"
                maxWidth="70%"
                style={{ height: '100%' }}
              >
                {highLight.title}
              </Box>
            </Box>
          </Link>
        </Box>
        {/* </li> */}
      </Box>
    ))}
  </Box>
);
