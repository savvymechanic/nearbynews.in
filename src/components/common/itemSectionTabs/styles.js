const styles = (theme) => ({
  HeadingBox: {
    display: 'flex',
    width: '100%',
    alignItems: 'flex-end',
    boxSizing: 'border-box',
    color: '#16418f',
  },
  Heading: {
    flexBasis: 'unset',

    display: 'inline-block',
    fontWeight: '700',
    textAlign: 'right',
    margin: ' 0rem 1rem 0rem 0rem',
  },

  ReadMore: {
    textAlign: 'right',
    marginRight: '10px',
    margin: 'auto',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    cursor: 'pointer',
    color: '#999999',
  },

  CardsContainer: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  SectionTitle: {
    fontWeight: '600',
    fontSize: '22px',
    fontFamily: 'Roboto, sans-serif',
  },
  HorizontalBar: {
    background: '#16418f',
    height: '1.5px',
    flexGrow: 2,
    margin: 'auto',
    marginRight: '1rem',
  },
  ItemCard: {
    marginTop: '1rem',
    display: 'flex',
    alignItems: 'stretch',
  },
});

export default styles;
