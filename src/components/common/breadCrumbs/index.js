/* eslint-disable react/prop-types */
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import Image from 'next/image';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { toPascalCase } from '../../../../utils/functions/stringManipulation';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useStyles = makeStyles((theme) => ({}));

const BreadCrumbs = () => {
  const classes = useStyles();
  const router = useRouter();
  const [breadcrumbs, setBreadcrumbs] = useState(null);
  const matches = useMediaQuery('(min-width:600px)');

  useEffect(() => {
    if (router) {
      const linkPath = router.asPath.split('/');
      linkPath.shift();

      const pathArray = linkPath.map((path, i) => {
        return {
          breadcrumb: path,
          href: '/' + linkPath?.slice(0, i + 1).join('/'),
        };
      });

      setBreadcrumbs(pathArray);
    }
  }, [router]);

  if (!breadcrumbs) {
    return null;
  }
  let stringOfBC;
  return (
    <nav>
      <Box
        className="breadcrumb"
        style={{ background: 'transparent', margin: '0rem', padding: '0rem' }}
        display="flex"
      >
        {breadcrumbs.map((breadcrumb, i) => {
          return matches ? (
            <Box key={breadcrumb.href} mx={1}>
              <Link href={breadcrumb.href} prefetch={false}>
                <a>
                  <Box display="flex" alignItems="center">
                    {toPascalCase(convertBreadcrumb(breadcrumb.breadcrumb))}
                    {breadcrumbs[i + 1] ? (
                      <ArrowForwardIosIcon style={{ fontSize: 11, marginLeft: '5px' }} />
                    ) : (
                      <div />
                    )}
                  </Box>
                </a>
              </Link>
            </Box>
          ) : (
            <Box key={breadcrumb.href} mx={1}>
              <Link href={breadcrumb.href} prefetch={false}>
                <a>
                  <Box display="flex" alignItems="center">
                    {breadcrumbs[i + 1] ? (
                      <>
                        {toPascalCase(convertBreadcrumb(breadcrumb.breadcrumb))}
                        <ArrowForwardIosIcon
                          style={{ fontSize: 11, marginLeft: '5px' }}
                        />
                      </>
                    ) : (
                      <div />
                    )}
                  </Box>
                </a>
              </Link>
            </Box>
          );
        })}
      </Box>
    </nav>
  );
};

export default BreadCrumbs;

const convertBreadcrumb = (string) => {
  return string
    .replace(/-/g, ' ')
    .replace(/oe/g, 'ö')
    .replace(/ae/g, 'ä')
    .replace(/ue/g, 'ü')
    .toUpperCase();
};
