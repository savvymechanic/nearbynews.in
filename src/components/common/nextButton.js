import React, { useEffect } from 'react';
import ArrowForwardIosOutlinedIcon from '@material-ui/icons/ArrowForwardIosOutlined';

const NextButton = (props) => {
  const { className, style, onClick } = props;
  useEffect(() => {
    console.log(props);
  });
  return (
    <div className={className} style={{ ...style, display: 'block' }} onClick={onClick}>
      <ArrowForwardIosOutlinedIcon />
    </div>
  );
};

export default NextButton;
