import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { Button } from '@material-ui/core';

export const SampleNextArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <div
      style={{
        fontSize: 0,
        lineHeight: 0,
        position: 'absolute',
        top: '50%',
        display: 'block',
        padding: 0,
        transform: 'translate(0, -50%)',
        cursor: 'pointer',
        color: 'transparent',
        border: 'none',
        outline: 'none',
        background: 'transparent',
        right: '-50px',
      }}
      onClick={onClick}
    >
      <Button
        variant="contained"
        style={{ background: 'transparent' }}
        disableElevation={true}
      >
        <ChevronRightIcon fontSize="small" />
      </Button>
    </div>
  );
};

export const SamplePrevArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <div
      style={{
        fontSize: 0,
        lineHeight: 0,
        position: 'absolute',
        top: '50%',
        display: 'block',
        padding: 0,
        transform: 'translate(0, -50%)',
        cursor: 'pointer',
        border: 'none',
        outline: 'none',
        left: '-50px',
        zIndex: 10,
      }}
      onClick={onClick}
    >
      <Button
        variant="contained"
        style={{ background: 'transparent' }}
        disableElevation={true}
      >
        <ChevronLeftIcon fontSize="small" />
      </Button>
    </div>
  );
};
