import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Image from 'next/image';

const SmallNewsItemCard = (props) => {
  const useStyles = makeStyles((theme) => ({
    title: {
      fontWeight: 'bolder',
      fontFamily: 'Verdana',
    },
    date: {
      color: '#B5B5B5',
    },
    img: {
      height: '6em',
      width: 'inherit', 
    },
  }));

  const classes = useStyles();
  return (
    <div className="row">
      <div className="col-3">
        <Image
          src={props.image}
          className={classes.img}
        />
      </div>
      <div className="col-9">
        <div className="col-12">
          <p>{props.title}</p>
        </div>
        <div className="col-4">
          <p className={classes.date}>{new Date(props.date).toDateString()}</p>
        </div>
      </div>
    </div>
  );
};

SmallNewsItemCard.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  author: PropTypes.string,
  date: PropTypes.string,
  category: PropTypes.string,
};

export default SmallNewsItemCard;
