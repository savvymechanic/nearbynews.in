/* eslint-disable react/prop-types */
import { Box, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { motion } from 'framer-motion';
import Image from 'next/image';
import Link from 'next/link';
import PropTypes from 'prop-types';
import React from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useStyles = makeStyles((theme) => ({
  parentBox: {
    //color: "#607d8b",
    //background: "red",
    fontFamily: 'Verdana, Geneva, sans-serif',
    fontSize: '14px',
    fontWeight: 400,
    height: '100%',
    lineHeight: '21px',
    margin: '0.5rem',
    padding: '0.35rem',
    //width: "300px",
    cursor: 'pointer',
    minHeight: '16rem',
    borderRadius: '0px',
    // boxShadow: '0 1px 3px 0 rgba(0,0,0,0.07)',
    backgroundColor: '#fff',
    '&:hover': {
      boxShadow: '0 6px 12px 0 rgba(0,0,0,0.3)',
    },
  },
  title: {
    color: '#16418f',
    fontFamily: 'Roboto, sans-serif',
    fontSize: '15px',
    fontWeight: 800,
    //height: "56px",
    lineHeight: '19.5px',
    //marginTop: "5px",
    //padding: "15px 8px 15px 20px;",
    // width: "216px"
  },
  contentBox: {
    marginTop: '5px',
    padding: '15px 8px 5px 20px;',
  },
  tagBox: {
    backgroundColor: '#FFDD0E',
    color: 'black',
    fontFamily: '"Open Sans", arial, sans-serif',
    fontSize: '10px',
    fontWeight: 600,
    height: '17px',
    lineHeight: '10px',
    margin: '0px 5px 0px 0px',
    padding: '3px 6px 4px',
    width: 'fit-content',
    marginTop: '-17px',
    zIndex: 1,
    position: 'relative',
    top: '0px',
  },
  imageBox: {
    borderRadius: '0',
  },
}));

const HomeItemCard = ({
  image,
  tag,
  title,
  city,
  slug,
  sectionSlug,
  authorName,
  date,
  isArchive,
}) => {
  const classes = useStyles();
  const matches = useMediaQuery('(min-width:600px)');

  return (
    <motion.div
      whileHover={{ scale: 1.05 }}
      transformTemplate={(props, transform) =>
        // Disable GPU acceleration to prevent blurry text
        transform.replace(' translateZ(0)', '')
      }
    >
      <Link
        as={`/${city}/${sectionSlug}/${slug}`}
        href={`/[city]/[${sectionSlug}]/[slug]`}
        prefetch={false}
      >
        <Paper elevation={5} className={classes.parentBox}>
          <Box>
            <Image
              src={
                image ||
                'https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png'
              }
              alt={title}
              layout="responsive"
              width={280}
              height={190}
              //  borderRadius="1rem"
              className={classes.imageBox}
              loading="eager"
              priority
            />
            <Box className={classes.tagBox}>{tag}</Box>
          </Box>
          <Box
            className={classes.contentBox}
            style={{ minHeight: matches ? '6rem' : '0rem' }}
          >
            <Box className={classes.title}>{title}</Box>
            {/* {authorName &&
            date &&
            isArchive &&
            (sectionSlug === 'cityspeaks' || sectionSlug === 'stories') ? (
              <Box pt={1} display="flex" fontSize="0.6rem">
                <Box fontWeight={500}>{`${authorName + ' '} `}</Box>
                <Box>{` - ${date}`}</Box>
              </Box>
            ) : (
              <div />
            )} */}
          </Box>
        </Paper>
      </Link>
    </motion.div>
  );
};

HomeItemCard.propTypes = {
  city: PropTypes.string,
  sectionSlug: PropTypes.string,
  image: PropTypes.string,
  tag: PropTypes.string,
  title: PropTypes.string,
  slug: PropTypes.string,
};

HomeItemCard.defaultProps = {
  image:
    'https://res.cloudinary.com/nearbynews/images/v1605014905/wp/settl.1/settl.1-1200x513.jpg',
  tag: 'Adminstration',
  title: 'This is the Log Title OF the Article',
  slug: '#',
  city: '#',
  sectionSlug: '#',
};

export default HomeItemCard;
