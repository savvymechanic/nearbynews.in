import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  img: {
    height: '20.75em',
    width: 'inherit',
  },
  author: {
    fontSize: '12px',
    fontWeight: 'bold',
    color: 'black',
  },

  title: {
    fontWeight: 'bolder',
    fontSize: '26px',
    fontFamily: 'Verdana',
  },
  date: {
    color: '#B5B5B5',
  },
  category: {
    position: 'absolute',
    top: '18.75em',
    width: 'fit-content',
    height: '2em',
    backgroundColor: 'black',
    paddingLeft: '0.25em',
    paddingRight: '0.25em',
    color: 'white',
  },
  categoryText: {
    fontSize: '15px',
    paddingLeft: '0.35em',
  },
}));

const NewsItemCard = (props) => {
  //console.log(props);
  const classes = useStyles();
  return (
    <div className="row">
      <div className="col-12">
        <Image className={classes.img} src={props.image}  />
        <div className={classes.category}>
          <p>{props.category}</p>
        </div>
      </div>
      <div className="col-12">
        <p className={classes.title}>{props.title}</p>
      </div>
      {props.section !== 'Newsletters' ? (
        <div style={{ display: 'contents' }}>
          <div className="col-3">
            <p className={classes.author}>{props.author}</p>
          </div>
          <div className="col-3 offset-1">
            <p className={classes.date}>{new Date(props.date).toDateString()}</p>
          </div>
        </div>
      ) : null}
    </div>
  );
};

NewsItemCard.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  author: PropTypes.string,
  date: PropTypes.string,
  category: PropTypes.string,
  section: PropTypes.string,
};

export default NewsItemCard;
