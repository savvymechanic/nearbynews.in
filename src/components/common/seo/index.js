import PropTypes from 'prop-types';
import React from 'react';
import Head from 'next/head';

export const SEO = ({ description, uri, title, image }) => {
  const lang = 'en';
  const metaDescription = description;
  const canonical = `https://www.nearbynews.in/${uri}`;
  const ogUrl = `https://www.nearbynews.in/${uri}`;

  return (
    <Head>
      <title>{title}</title>
      <meta name="title" content={title} />
      <meta name="description" content={description} />

      {/* <!-- Open Graph / Facebook --> */}
      <meta property="og:type" content="website" />
      <meta property="og:url" content={ogUrl} />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />

      {/* <!-- Twitter --> */}
      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:url" content={ogUrl} />
      <meta property="twitter:title" content={title} />
      <meta property="twitter:description" content={description} />
      <meta property="twitter:image" content={image} />
    </Head>
  );
};

SEO.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  uri: PropTypes.string.isRequired,
  image: PropTypes.string,
};

export default SEO;
