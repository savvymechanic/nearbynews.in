import { Box } from '@material-ui/core';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { IP_URL } from '../../../util/constants';

const WeatherCard = () => {
  const [temp, setTemp] = useState(' ');
  const [description, setDescription] = useState('');
  const [city, setCity] = useState('');

  useEffect(() => {
    axios.get(`${IP_URL}`).then((ip_response) => {
      let ipDetails = ip_response.data;
      let city = ipDetails.city;
      // let region = ipDetails.region;
      // let country = ipDetails.country;
      axios
        .get(
          `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=f03b21af3bfe7b71292d314d12077aa3`,
        )
        .then((response) => {
          let weather = response.data;
          setTemp(Math.round(weather.main.temp - 273.15));
          setDescription(weather.weather[0].description);
          setCity(city);
        });
    });
  }, []);
  return (
    <Box display="flex" mr={1}>
      <Box fontSize="2.5rem">{temp}</Box>
      <Box fontSize="1rem">{temp === ' ' ? ' ' : 'o'}</Box>
      <Box ml={2} mt={1}>
        <Box fontWeight="700">{city}</Box>
        <Box>{description}</Box>
      </Box>
    </Box>
  );
};

export default WeatherCard;
