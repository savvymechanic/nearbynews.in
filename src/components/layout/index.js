/* eslint-disable react/prop-types */
import React from 'react';
import Header from './Header';
import Footer from './footer';
// import Footer from "./Footer";
import { Box } from '@material-ui/core';
import FooterSubsSection from '../sections/footerSubsSection';
import GA from '../common/GA';
export default function Layout(props) {
  return (
    <GA>
      <div style={{ background: 'white' }}>
        <Header />
        <Box pt={3}>{props.children}</Box>
        <FooterSubsSection />
        <Footer />
      </div>
    </GA>
  );
}
