import { Box, Menu, Paper } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import { ArrowDropDown } from '@material-ui/icons';
import { useRouter } from 'next/router';
import React from 'react';

export default function TerritoryToggle(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const router = useRouter();
  const { city = 'hyderabad' } = router.query;
  const [image, setImage] = React.useState(
    city?.replace(/(\w)(\w*)/g, function (g0, g1, g2) {
      return g1.toUpperCase() + g2.toLowerCase();
    }),
  );

  const changeImg = (city) => {
    setAnchorEl(null);
    setImage(city);
    router.push(`/${city?.toLowerCase()}/stories`);
  };

  return (
    <div>
      <Paper
        onClick={(event) => setAnchorEl(event.currentTarget)}
        style={{ background: props.isMobile ? ' #f1a104' : '#16418f', cursor: 'pointer' }}
      >
        <Box px={1} style={{ color: 'whitesmoke', display: 'flex' }}>
          {image}
          <ArrowDropDown color="inherit" style={{ verticalAlign: 'middle' }} />
        </Box>
      </Paper>

      <Menu
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        getContentAnchorEl={null}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        transformOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        {territories?.map((item, i) => (
          <MenuItem onClick={() => changeImg(item.city)} value={i} key={item.city}>
            {item.city}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
}

const territories = [
  { city: 'Hyderabad' },
  { city: 'Mumbai' },
  { city: 'Chennai' },
  { city: 'Delhi' },
  { city: 'Bangalore' },
];
