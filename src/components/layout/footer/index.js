import { Box, makeStyles } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Image from 'next/image';
import React from 'react';
import { SOCIAL_ICON_LINKS, SOCIAL_LINKS } from '../../../util/constants';
import styles from './style';
const useStyles = makeStyles(styles);

const Footer = (props) => {
  const classes = useStyles();
  return (
    <Box display="flex" flexDirection="column">
      <Box display="flex" flexDirection="column" alignItems="center" m={2}>
        <Box style={{ fontWeight: 'bolder', fontSize: '1.5rem', color: 'black' }}>
          <Image
            src="https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png"
            alt="logo"
            layout="fixed"
            width={150}
            height={50}
          />
        </Box>
        <Box>hi@nearbynews.in</Box>
        <Box>Gachibowli, Hyderabad - 500032</Box>
        <Box display="flex" mt={3}>
          <a href={SOCIAL_LINKS.FACEBOOK} target="_blank" rel="noreferrer">
            <Avatar
              style={{ width: '1rem', height: '1rem', margin: '0px 8px' }}
              variant="square"
              src={SOCIAL_ICON_LINKS.FACEBOOK}
            />
          </a>
          <a href={SOCIAL_LINKS.TWITTER} target="_blank" rel="noreferrer">
            <Avatar
              style={{ width: '1rem', height: '1rem', margin: '0px 8px' }}
              variant="square"
              src={SOCIAL_ICON_LINKS.TWITTER}
            />
          </a>
          <a href={SOCIAL_LINKS.LINKEDIN} target="_blank" rel="noreferrer">
            <Avatar
              style={{ width: '1rem', height: '1rem', margin: '0px 8px' }}
              variant="square"
              src={SOCIAL_ICON_LINKS.LINKEDIN}
            />
          </a>
          <a href={SOCIAL_LINKS.INSTAGRAM} target="_blank" rel="noreferrer">
            <Avatar
              style={{ width: '1rem', height: '1rem', margin: '0px 8px' }}
              variant="square"
              src={SOCIAL_ICON_LINKS.INSTAGRAM}
            />
          </a>
        </Box>
      </Box>

      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        textAlign="center"
        padding={2}
        fontSize="0.8rem"
        style={{ backgroundColor: '#16418F', color: 'white' }}
      >
        nearby news - currently serving Hyderabad community {new Date().getFullYear()}
      </Box>
    </Box>
  );
};

export default Footer;
