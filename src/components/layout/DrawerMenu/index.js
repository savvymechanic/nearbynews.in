import React, { Fragment } from 'react';
import {
  Avatar,
  Box,
  IconButton,
  List,
  ListItem,
  ListItemText,
  makeStyles,
} from '@material-ui/core';
import Link from 'next/link';

import { SOCIAL_ICON_LINKS, SOCIAL_LINKS } from '../../../util/constants';
import CloseIcon from '@material-ui/icons/Close';
import { useRouter } from 'next/router';
import TerritoryToggle from '../TerritoryToggle';

const ListItemLink = (props) => {
  return (
    <ListItem button component="a" {...props} onClick={() => props.toggleDrawer()} />
  );
};

const useStyles = makeStyles({
  primary: {
    fontSize: '25px',
  },
  categeory: {
    padding: '0.2rem 0rem',
  },
});

const DrawerMenu = (props) => {
  const drawerStyle = {
    padding: '20px',
    fontSize: '20px',
  };
  const router = useRouter();
  const { city = 'hyderabad' } = router.query;
  const classes = useStyles();

  return (
    <div style={drawerStyle}>
      <div style={{ float: 'right' }}>
        <ListItem button onClick={props.toggleDrawer}>
          <CloseIcon fontSize="large" onClick={props.toggleDrawer} />
        </ListItem>
      </div>
      <List component="nav" aria-label="drawer-menu">
        <Box
          style={{
            display: 'flex',
            flexDirection: 'column',
          }}
          mt={4}
        >
          <ListItemLink href={`/`} toggleDrawer={props.toggleDrawer}>
            <Link as={`/`} href={`/`} prefetch={false}>
              <Box className={classes.categeory}>Home</Box>
            </Link>
          </ListItemLink>

          <ListItemLink href={`/${city}/newsletters`} toggleDrawer={props.toggleDrawer}>
            <Link
              as={`/${city}/newsletters`}
              href={`/[city]/newsletters`}
              prefetch={false}
            >
              <Box className={classes.categeory}>Newsletters</Box>
            </Link>
          </ListItemLink>

          <ListItemLink href={`/${city}/stories`} toggleDrawer={props.toggleDrawer}>
            <Link as={`/${city}/stories`} href={`/[city]/stories`} prefetch={false}>
              <Box className={classes.categeory}>Stories</Box>
            </Link>
          </ListItemLink>

          <ListItemLink
            href={`/${city}/stories/categories/lifestyle`}
            toggleDrawer={props.toggleDrawer}
          >
            <Link
              as={`/${city}/stories/categories/lifestyle`}
              href={`/[city]/stories/categories/[category]`}
              prefetch={false}
            >
              <Box className={classes.categeory}>LifeStyle</Box>
            </Link>
          </ListItemLink>

          <ListItemLink
            href={`/${city}/stories/categories/business`}
            toggleDrawer={props.toggleDrawer}
          >
            <Link
              as={`/${city}/stories/categories/business`}
              href={`/[city]/stories/categories/[category]`}
              prefetch={false}
            >
              <Box className={classes.categeory}>Business</Box>
            </Link>
          </ListItemLink>

          <ListItemLink
            href={`/${city}/stories/categories/administration`}
            toggleDrawer={props.toggleDrawer}
          >
            <Link
              as={`/${city}/stories/categories/administration`}
              href={`/[city]/stories/categories/[category]`}
              prefetch={false}
            >
              <Box className={classes.categeory}>Administration</Box>
            </Link>
          </ListItemLink>

          <ListItemLink
            href={`/${city}/stories/categories/politics`}
            toggleDrawer={props.toggleDrawer}
          >
            <Link
              as={`/${city}/stories/categories/politics`}
              href={`/[city]/stories/categories/[category]`}
              prefetch={false}
            >
              <Box className={classes.categeory}>Politics</Box>
            </Link>
          </ListItemLink>

          <ListItemLink
            href={`/${city}/stories/categories/entertainment`}
            toggleDrawer={props.toggleDrawer}
          >
            <Link
              as={`/${city}/stories/categories/entertainment`}
              href={`/[city]/stories/categories/[category]`}
              prefetch={false}
            >
              <Box className={classes.categeory}>Entertainment</Box>
            </Link>
          </ListItemLink>

          <ListItemLink
            href={`/${city}/stories/categories/startups`}
            toggleDrawer={props.toggleDrawer}
          >
            <Link
              as={`/${city}/stories/categories/startups`}
              href={`/[city]/stories/categories/[category]`}
              prefetch={false}
            >
              <Box className={classes.categeory}>Startups</Box>
            </Link>
          </ListItemLink>
          <ListItemLink toggleDrawer={() => {}}>
            <TerritoryToggle isMobile />
          </ListItemLink>
        </Box>
      </List>

      <div style={{ marginTop: '20px' }}>
        <Box display="flex">
          <a href={SOCIAL_LINKS.FACEBOOK} target="_blank" rel="noreferrer">
            <Avatar
              style={{ width: '1.5rem', height: '1.5rem', margin: '0px 8px' }}
              variant="square"
              src={SOCIAL_ICON_LINKS.FACEBOOK}
            />
          </a>
          <a href={SOCIAL_LINKS.TWITTER} target="_blank" rel="noreferrer">
            <Avatar
              style={{
                color: 'white',
                width: '1.5rem',
                height: '1.5rem',
                margin: '0px 8px',
              }}
              variant="square"
              src={SOCIAL_ICON_LINKS.TWITTER}
            />
          </a>
          <a href={SOCIAL_LINKS.LINKEDIN} target="_blank" rel="noreferrer">
            <Avatar
              style={{ width: '1.5rem', height: '1.5rem', margin: '0px 8px' }}
              variant="square"
              src={SOCIAL_ICON_LINKS.LINKEDIN}
            />
          </a>
          <a href={SOCIAL_LINKS.INSTAGRAM} target="_blank" rel="noreferrer">
            <Avatar
              style={{ width: '1.5rem', height: '1.5rem', margin: '0px 8px' }}
              variant="square"
              src={SOCIAL_ICON_LINKS.INSTAGRAM}
            />
          </a>
        </Box>
      </div>
    </div>
  );
};

export default DrawerMenu;
