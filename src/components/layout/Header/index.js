/* eslint-disable react/prop-types */
import { Box, Container } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
// nodejs library to set properties for components
import { makeStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/icons/Menu';
import dynamic from 'next/dynamic';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { SOCIAL_ICON_LINKS, SOCIAL_LINKS } from '../../../util/constants';
import TerritoryToggle from '../TerritoryToggle';
import styles from './style';
import DrawerMenu from '../DrawerMenu';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useStyles = makeStyles(styles);

const WeatherCard = dynamic(() => import('../weatherCard'), { ssr: false });

const Header = (props) => {
  const matches = useMediaQuery('(min-width:600px)');
  const classes = useStyles();
  const router = useRouter();
  const { city } = router.query;
  const [state, setState] = useState({
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    console.log('Drawer Closed');
    setState({ ...state, [anchor]: open });
  };

  return (
    <Box>
      <Container style={{ paddingTop: '1rem', marginBottom: '0rem' }}>
        <Hidden only={['xs', 'sm']}>
          <Box display="flex" justifyContent="space-between" alignItems="center">
            <Link as="/" href="/" prefetch={false}>
              <div className={classes.logoSmall}>
                <Image
                  src="/logo/nnlogo.webp"
                  alt="logo"
                  layout="responsive"
                  height={100}
                  width={100}
                />
              </div>
            </Link>
            <Box>
              <WeatherCard />
            </Box>
            <Box display="flex" alignItems="center">
              <Box display="flex">
                <Link href="#newsletters" prefetch={false}>
                  <Box className={classes.menuItem}>Newsletters</Box>
                </Link>
                <Link href="#stories" prefetch={false}>
                  <Box className={classes.menuItem}>Top Stories</Box>
                </Link>
                <Link href="#cityspeaks" prefetch={false}>
                  <Box className={classes.menuItem}>City Speaks</Box>
                </Link>
              </Box>
              <Box display="flex">
                <a href={SOCIAL_LINKS.FACEBOOK} target="_blank" rel="noreferrer">
                  <Avatar
                    style={{ width: '1rem', height: '1rem', margin: '0px 8px' }}
                    variant="square"
                    src={SOCIAL_ICON_LINKS.FACEBOOK}
                  />
                </a>
                <a href={SOCIAL_LINKS.TWITTER} target="_blank" rel="noreferrer">
                  <Avatar
                    style={{ width: '1rem', height: '1rem', margin: '0px 8px' }}
                    variant="square"
                    src={SOCIAL_ICON_LINKS.TWITTER}
                  />
                </a>
                <a href={SOCIAL_LINKS.LINKEDIN} target="_blank" rel="noreferrer">
                  <Avatar
                    style={{ width: '1rem', height: '1rem', margin: '0px 8px' }}
                    variant="square"
                    src={SOCIAL_ICON_LINKS.LINKEDIN}
                  />
                </a>
                <a href={SOCIAL_LINKS.INSTAGRAM} target="_blank" rel="noreferrer">
                  <Avatar
                    style={{ width: '1rem', height: '1rem', margin: '0px 8px' }}
                    variant="square"
                    src={SOCIAL_ICON_LINKS.INSTAGRAM}
                  />
                </a>
              </Box>
            </Box>
          </Box>
        </Hidden>
      </Container>
      {!matches ||
      router?.asPath === '/' ||
      router?.asPath.includes('/newsletters/') ||
      router?.asPath.includes('#') ? (
        <div />
      ) : (
        <Box
          style={{
            backgroundColor: '#f1f1f1',
            color: '#4b4b4b',
            fontFamily: '"Open Sans", sans-serif',
            fontSize: '16px',
            fontWeight: 400,
            width: '100%',
            padding: '0.6rem 0rem',
            boxShadow: '0 1px 3px 0 rgba(0,0,0,0.07)',
            backgroundColor: '#fff',
          }}
          mt={4}
        >
          <Container>
            <Box
              style={{
                display: 'flex',
                fontWeight: '600',
                justifyContent: 'space-evenly',
              }}
            >
              <Link as={`/`} href={`/`} prefetch={false}>
                <Box className={classes.categeory}>Home</Box>
              </Link>
              <Link
                as={`/${city}/newsletters`}
                href={`/[city]/newsletters`}
                prefetch={false}
              >
                <Box
                  style={{
                    color: router?.asPath.includes('newsletters') ? '#16418f' : '',
                  }}
                  className={classes.categeory}
                >
                  Newsletters
                </Box>
              </Link>
              <Link as={`/${city}/stories`} href={`/[city]/stories`} prefetch={false}>
                <Box
                  style={{
                    color: router?.asPath.includes('stories') ? '#16418f' : '',
                  }}
                  className={classes.categeory}
                >
                  Stories
                </Box>
              </Link>
              <Link
                as={`/${city}/stories/categories/lifestyle`}
                href={`/[city]/stories/categories/[category]`}
                prefetch={false}
              >
                <Box
                  style={{
                    color: router?.asPath.includes('lifestyle') ? '#16418f' : '',
                  }}
                  className={classes.categeory}
                >
                  LifeStyle
                </Box>
              </Link>
              <Link
                as={`/${city}/stories/categories/business`}
                href={`/[city]/stories/categories/[category]`}
                prefetch={false}
              >
                <Box
                  style={{
                    color: router?.asPath.includes('business') ? '#16418f' : '',
                  }}
                  className={classes.categeory}
                >
                  Business
                </Box>
              </Link>
              <Link
                as={`/${city}/stories/categories/administration`}
                href={`/[city]/stories/categories/[category]`}
                prefetch={false}
              >
                <Box
                  style={{
                    color: router?.asPath.includes('administration') ? '#16418f' : '',
                  }}
                  className={classes.categeory}
                >
                  Administration
                </Box>
              </Link>
              <Link
                as={`/${city}/stories/categories/politics`}
                href={`/[city]/stories/categories/[category]`}
                prefetch={false}
              >
                <Box
                  style={{
                    color: router?.asPath.includes('politics') ? '#16418f' : '',
                  }}
                  className={classes.categeory}
                >
                  Politics
                </Box>
              </Link>
              <Link
                as={`/${city}/stories/categories/entertainment`}
                href={`/[city]/stories/categories/[category]`}
                prefetch={false}
              >
                <Box
                  style={{
                    color: router?.asPath.includes('entertainment') ? '#16418f' : '',
                  }}
                  className={classes.categeory}
                >
                  Entertainment
                </Box>
              </Link>
              <Link
                as={`/${city}/stories/categories/startups`}
                href={`/[city]/stories/categories/[category]`}
                prefetch={false}
              >
                <Box
                  style={{
                    color: router?.asPath.includes('startups') ? '#16418f' : '',
                  }}
                  className={classes.categeory}
                >
                  Startups
                </Box>
              </Link>
              <TerritoryToggle />
            </Box>
          </Container>
        </Box>
      )}
      <Hidden only={['md', 'lg', 'xl']}>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Link as="/" href="/" prefetch={false}>
            <Image
              src="https://res.cloudinary.com/nearbynews/image/upload/v1607713659/nnlogo_xte8vp.png"
              layout="responsive"
              height={100}
              width={100}
            />
          </Link>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={toggleDrawer('right', true)}
          >
            <Menu />
          </IconButton>
        </div>
      </Hidden>

      <Drawer
        anchor="right"
        open={state.right}
        onClose={toggleDrawer('right', false)}
        variant="temporary"
        classes={{ paper: classes.drawerMenuPaper }}
      >
        <DrawerMenu
          className={classes.drawerMenuParent}
          toggleDrawer={toggleDrawer('right', false)}
        />
      </Drawer>
    </Box>
  );
};

export default Header;
