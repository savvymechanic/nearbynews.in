const styles = (theme) => ({
  root: {
    //  height: '100px',
  },
  appBar: {
    display: 'flex',
    marginBottom: '20px',
    width: '100%',
    maxWidht: '1280px',
    transition: 'all 150ms ease 0s',
    alignItems: 'center',
    flexFlow: 'row nowrap',
    justifyContent: 'flex-start',
    position: 'relative',
    zIndex: 'unset',
  },
  absolute: {
    position: 'absolute',
    zIndex: '1100',
  },
  fixed: {
    position: 'fixed',
    zIndex: '1100',
  },
  container: {
    paddingRight: '15px',
    paddingLeft: '15px',
    marginRight: 'auto',
    marginLeft: 'auto',
    width: '100%',
    '@media (min-width: 576px)': {
      maxWidth: '540px',
    },
    '@media (min-width: 768px)': {
      maxWidth: '720px',
    },
    '@media (min-width: 992px)': {
      maxWidth: '960px',
    },
    '@media (min-width: 1200px)': {
      maxWidth: '1140px',
    },
    minHeight: '50px',
    flex: '1',
    alignItems: 'center',
    justifyContent: 'space-between',
    display: 'flex',
    flexWrap: 'nowrap',
  },
  flex: {
    flex: 1,
  },

  appResponsive: {
    margin: '20px 10px',
  },

  drawerPaper: {
    border: 'none',
    bottom: '0',
    transitionProperty: 'top, bottom, width',
    transitionDuration: '.2s, .2s, .35s',
    transitionTimingFunction: 'linear, linear, ease',
    position: 'fixed',
    display: 'block',
    top: '0',
    height: '100vh',
    right: '0',
    left: 'auto',
    visibility: 'visible',
    overflowY: 'visible',
    borderTop: 'none',
    textAlign: 'left',
    paddingRight: '5px',
    paddingLeft: '5px',
    transition: 'all 0.33s cubic-bezier(0.685, 0.0473, 0.346, 1)',
  },

  categeory: {
    cursor: 'pointer',
  },

  menuItem: {
    color: '#f1a104',
    fontSize: '18px',
    fontWeight: 700,
    minWidth: '100px',
    padding: '1rem 1.5rem',
    cursor: 'pointer',
  },

  drawerMenuPaper: {
    background: 'rgb(22,65,143)',
    color: 'white',
  },


  
  logoSmall: {
    height: 'auto',
    width: '20%',
    cursor: 'pointer',
  },
});
export default styles;
