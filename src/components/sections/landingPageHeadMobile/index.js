import React from 'react';
import { Box, Container, Grid, Paper, Typography } from '@material-ui/core';
import Link from 'next/link';
import Image from 'next/image';
import MiniSubsBox from '../../misc/miniSubsBox';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const LandingPageHeadMobile = (props) => {
  const matches = useMediaQuery('(min-width:600px)');

  return (
    <Box mt={-2} ml={0} mb={2}>
      <Grid container spacing={0} justify="center" alignContent="center">
        <Grid item xs={10} style={{ display: 'flex', alignItems: 'center' }}>
          <Box pl={1} my={5}>
            <Box
              style={{
                color: 'rgb(22,65,143)',
                lineHeight: '37px',
                fontSize: '25px',
                font: 'normal normal normal  roboto-bold,roboto,sans-serif',
                fontWeight: '600',
              }}
            >
              Get your local newspaper delivered to your
              <span
                style={{
                  color: '#f1a104',
                }}
              >
                {' Email'}
              </span>
              {' at'}
              <span
                style={{
                  color: '#f1a104',
                }}
              >
                {' 7am'}
              </span>
              {' everyday'}
            </Box>
            <Box
              style={{
                width: '98%',
                height: 'auto',
              }}
              pt={4}
            >
              <Image src="/illustrations/landingImage.svg" layout="fill" />
            </Box>
            <Box
              style={{
                fontSize: '13px',
                fontWeight: 500,
                lineHeight: '20px',
              }}
              mt={3}
              px={3}
            >
              Experience the modern way of reading newspapers Zero Spam, no app needed and
              free forever!
            </Box>
            <Box>
              <Box pt={0} px={2}>
                <MiniSubsBox />
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default LandingPageHeadMobile;
