import { Box, Container, Paper, Typography, makeStyles } from '@material-ui/core';
import Link from 'next/link';
import React from 'react';
import NewsItemCard from '../../common/newsItemCard/newsItemCard';
import SmallNewsItemCard from '../../common/smallNewsItemCard/smallNewsItemCard';

const CitySpeaksSection = (props) => {
  const { citySpeaks, city } = props;

  const useStyles = makeStyles((theme) => ({
    newsItem: {
      paddingRight: '2em',
    },
    heading: {
      backgroundColor: '#4CAF50',
      height: '3.1325em',
      width: '12em',
    },
    headingText: {
      position: 'absolute',
      fontFamily: 'Roboto',
      top: '0.5em',
      left: '2em',
      fontSize: '18px',
    },
    border: {
      borderBottom: '3px solid #4CAF50',
    },
  }));

  const classes = useStyles();

  return (
    <Container>
      <Box mt={5}>
        <div className="row" style={{ paddingBottom: '6em' }}>
          <div className={`${classes.border} col-12`}>
            <div className={classes.heading}>
              <p className={classes.headingText}>City Speaks</p>
            </div>
          </div>
        </div>
        <div className="row">
          {citySpeaks.map((cityspeak, i) => {
            return i < 2 ? (
              <div className={`${classes.newsItem} col-6`} key={i}>
                <NewsItemCard
                  title={cityspeak.title}
                  image={cityspeak.imageUrl}
                  author={cityspeak.authorName}
                  date={cityspeak.date}
                  category={cityspeak.category}
                  section="Cityspeaks"
                />
              </div>
            ) : null;
          })}
        </div>
        <br />
        <div className="row">
          {citySpeaks.map((citySpeak, i) => {
            return i >= 3 && i <= 6 ? (
              <div style={{ display: 'contents' }}>
                <div className="col-6">
                  <SmallNewsItemCard
                    title={citySpeak.title}
                    image={citySpeak.imageUrl}
                    date={citySpeak.date}
                  />
                </div>
              </div>
            ) : null;
          })}
        </div>
        <Typography variant="h6">
          {' '}
          <Link as={`/${city}/cityspeaks`} href="/[city]/cityspeaks" prefetch={false}>
            View All City Speaks
          </Link>{' '}
        </Typography>
      </Box>
    </Container>
  );
};

export default CitySpeaksSection;
