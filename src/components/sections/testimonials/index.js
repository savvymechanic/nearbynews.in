/* eslint-disable react/no-unescaped-entities */
import { Box, Container, Grid, makeStyles, Paper, Button } from '@material-ui/core';
import React from 'react';
import Slider from 'react-slick';
import { SampleNextArrow, SamplePrevArrow } from '../../common/sliderArrows';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Image from 'next/image';

// import 'slick-carousel/slick/slick.css';
// import 'slick-carousel/slick/slick-theme.css';

const Testimonials = () => {
  const useStyles = makeStyles((theme) => ({
    Box: {
      color: ' #16418f',
    },
    h3: {
      color: ' #16418f',
    },

    imgStyle: {
      height: '4rem',
    },
  }));
  const classes = useStyles();
  const matches = useMediaQuery('(min-width:600px)');

  let settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplaySpeed: 2500,
    autoplay: true,
    arrows: matches ? true : false,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  return (
    <Container>
      <Box my={8}>
        <Slider {...settings}>
          <Box>
            <Box
              style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'center',
              }}
            >
              <Paper
                style={{
                  width: matches ? '57vw' : '80vw',
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: ' #16418f',
                }}
                elevation={0}
              >
                <div className={classes.imgStyle}>
                  <Image
                    src="https://static.wixstatic.com/media/43b732_5ee922a82f804a51a089e60a81f38e2c~mv2.png"
                    alt="testimonials"
                    layout="fill"
                  />
                </div>
                <Box
                  style={{
                    textAlign: 'center',
                    textJustify: 'inter-word',
                    fontSize: '1.1rem',
                  }}
                >
                  "I was a morning newspaper person earlier but moved to social media
                  slowly and unknowingly. Now this digital e-paper on mail every day gives
                  me a combination of both the experiences"
                </Box>
                <h2 style={{ color: ' #16418f', fontSize: '1.4em' }}>
                  Niharika Gupta | Begumpet
                </h2>
              </Paper>
            </Box>
          </Box>

          <Box>
            <Box
              style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'center',
              }}
            >
              <Paper
                style={{
                  width: matches ? '57vw' : '80vw',
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                elevation={0}
              >
                <div className={classes.imgStyle}>
                  <Image
                    src="https://static.wixstatic.com/media/43b732_5ee922a82f804a51a089e60a81f38e2c~mv2.png"
                    layout="fill"
                    alt="testimonials"
                  />
                </div>
                <Box
                  style={{
                    textAlign: 'center',
                    textJustify: 'inter-word',
                    color: ' #16418f',
                    fontSize: '1.1rem',
                  }}
                >
                  "I consume State and National News from Apps and TV, but staying
                  connected with News around me and the city has always been a problem,
                  but not anymore because of Nearby News"
                </Box>
                <h2 style={{ color: ' #16418f', fontSize: '1.4em' }}>
                  Ujwal Gattupalli | Kukatpally{' '}
                </h2>
              </Paper>
            </Box>
          </Box>

          <Box>
            <Box
              style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'center',
              }}
            >
              <Paper
                style={{
                  width: matches ? '57vw' : '80vw',
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                elevation={0}
              >
                <div className={classes.imgStyle}>
                  <Image
                    src="https://static.wixstatic.com/media/43b732_5ee922a82f804a51a089e60a81f38e2c~mv2.png"
                    alt="testimonials" 
                    layout="fill"
                  />
                </div>

                <Box
                  style={{
                    textAlign: 'center',
                    textJustify: 'inter-word',
                    color: ' #16418f',
                    fontSize: '1.1rem',
                  }}
                >
                  "I am from North India who recently moved to Hyderabad for work, as
                  someone who loves to explore cities I find Nearby News very relevant for
                  events, updates and history of the city. I feel half Hyderabadi now"
                </Box>
                <h2 style={{ color: ' #16418f', fontSize: '1.4em' }}>
                  Navneet Kumar | Gachibowli{' '}
                </h2>
              </Paper>
            </Box>
          </Box>
        </Slider>
      </Box>
    </Container>
  );
};

export default Testimonials;
