import { Box, Container, Paper, Typography, makeStyles } from '@material-ui/core';
import Link from 'next/link';
import React from 'react';
import PropTypes from 'prop-types';
import NewsItemCard from '../../common/newsItemCard/newsItemCard';
import SmallNewsItemCard from '../../common/smallNewsItemCard/smallNewsItemCard';
import { BorderBottom } from '@material-ui/icons';

const StoriesSection = (props) => {
  const { stories, city } = props;
  const useStyles = makeStyles((theme) => ({
    newsItem: {
      paddingRight: '2em',
    },
    heading: {
      backgroundColor: '#F9C100',
      height: '3.1325em',
      width: '12em',
    },
    headingText: {
      position: 'absolute',
      fontFamily: 'Roboto',
      top: '0.5em',
      left: '2em',
      fontSize: '18px',
    },
    border: {
      borderBottom: '3px solid #F9C100',
    },
  }));

  const classes = useStyles();

  return (
    <Container>
      <div className="row" style={{ paddingBottom: '6em' }}>
        <div className={`${classes.border} col-12`}>
          <div className={classes.heading}>
            <p className={classes.headingText}>Recent Stories</p>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6 col-12">
          {stories[0] ? (
            <NewsItemCard
              title={stories[0]?.title}
              image={stories[0]?.imageUrl}
              date={stories[0]?.date}
              author={stories[0]?.authorName}
              category={stories[0]?.category}
              section="Stories"
            />
          ) : (
            <div />
          )}
        </div>
        <div className="col-lg-6 col-12">
          {stories ? (
            stories.map((story, i) => {
              return i > 0 && i < 5 ? (
                <div key={i} className="col-12">
                  <SmallNewsItemCard
                    title={story?.title}
                    image={story?.imageUrl}
                    date={story?.date}
                    category={story?.category}
                  />
                </div>
              ) : null;
            })
          ) : (
            <div />
          )}
        </div>
      </div>
      <Typography variant="h6">
        {' '}
        <Link as={`/${city}/stories`} href="/[city]/stories" prefetch={false}>
          View All Stories
        </Link>{' '}
      </Typography>
    </Container>
  );
};

export default StoriesSection;

StoriesSection.propTypes = {
  stories: PropTypes.array,
};
