import React from 'react';
import { Box, Container, Grid, Paper, Typography } from '@material-ui/core';
import Link from 'next/link';
import Image from 'next/image';
import MiniSubsBox from '../../misc/miniSubsBox';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const LandingPageHead = (props) => {
  const matches = useMediaQuery('(min-width:600px)');

  return (
    <Box px={matches ? 3 : 0} ml={matches ? 4 : 0} mb={2}>
      <Grid container spacing={matches ? 5 : 0} justify="center" alignContent="center">
        <Grid
          item
          xs={12}
          sm={12}
          md={6}
          style={{ display: 'flex', alignItems: 'center' }}
        >
          <Box pl={matches ? 2 : 1} my={matches ? 0 : 5}>
            <Box
              style={{
                color: 'rgb(22,65,143)',
                lineHeight: matches ? '67px' : '40px',
                fontSize: matches ? '45px' : '27px',
                font: 'normal normal normal  roboto-bold,roboto,sans-serif',
                fontWeight: '600',
              }}
            >
              Get your local newspaper delivered to your
              <span
                style={{
                  color: '#f1a104',
                }}
              >
                {' Email'}
              </span>
              {' at'}
              <span
                style={{
                  color: '#f1a104',
                }}
              >
                {' 7am'}
              </span>
              {' everyday'}
            </Box>
            <Box
              style={{
                fontSize: '17px',
                fontWeight: 400,
                lineHeight: '30px',
              }}
              mt={2}
            >
              Experience the modern way of reading newspapers - Zero Spam,
              <br /> no app needed and free forever!
            </Box>
            <Box>
              <Box pt={1} px={matches ? 0 : 2}>
                <MiniSubsBox />
              </Box>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Box
            ml={4}
            style={{
              width: '85%',
              height: 'auto',
            }}
          >
            <Image src="/illustrations/landingImage.svg" layout="responsive" height={200} width={200} />
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default LandingPageHead;
