import { Box, Container, makeStyles, Paper, Typography } from '@material-ui/core';
import Link from 'next/link';
import NewsItemCard from '../../common/newsItemCard/newsItemCard';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  newsItem: {
    paddingRight: '2em',
  },
  heading: {
    backgroundColor: '#607D8B',
    height: '3.1325em',
    width: '12em',
  },
  headingText: {
    position: 'absolute',
    fontFamily: 'Roboto',
    top: '0.5em',
    left: '2em',
    fontSize: '18px',
  },
  border: {
    borderBottom: '3px solid #607D8B',
  },
}));

const NewsLettersSection = (props) => {
  const { newsletters, city } = props;
  const classes = useStyles();
  return (
    <Container>
      <Box mt={5}>
        <div className="row" style={{ paddingBottom: '6em' }}>
          <div className={`${classes.border} col-12`}>
            <div className={classes.heading}>
              <p className={classes.headingText}>Newsletters</p>
            </div>
          </div>
        </div>
        <div className="row">
          {newsletters.map((newsletter, i) => {
            //console.log(newsletter);
            return (
              <div key={i} className={`${classes.newsItem} col-4`}>
                <NewsItemCard
                  title={newsletter.title}
                  image={newsletter.imageUrl}
                  section="Newsletters"
                  category={new Date(newsletter.date).toDateString()}
                ></NewsItemCard>
              </div>
            );
          })}
        </div>
        <Typography variant="h6">
          {' '}
          <Link as={`/${city}/newsletters`} href="/[city]/newsletters" prefetch={false}>
            View All Newsletter
          </Link>{' '}
        </Typography>
      </Box>
    </Container>
  );
};

export default NewsLettersSection;
