import React from 'react';
import { Box, Button, makeStyles, Container, Grid, Typography } from '@material-ui/core';
import MiniSubsBox from '../../misc/miniSubsBox';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useStyles = makeStyles({
  aboutUs: {
    color: '#16418f',
    fontFamily: 'roboto-bold, roboto, sans-serif',
    fontSize: '30px',
    fontWeight: 700,
    textAlign: 'center',
    margin: '2rem',
  },
});
export default function FooterSubsSection() {
  const classes = useStyles();
  const matches = useMediaQuery('(min-width:600px)');

  return (
    <Container maxWidth="sm">
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
        mb={4}
        mt={4}
        fontWeight={600}
        px={3}
      >
        <Box className={classes.aboutUs}>Subscribe to our Newsletter</Box>
        <MiniSubsBox isFooter />
      </Box>
    </Container>
  );
}
