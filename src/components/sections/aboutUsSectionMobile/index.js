import { Box, Container, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Image from 'next/image';
import React from 'react';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },

  title: {
    color: '#f1a104',
    fontFamily: 'roboto-bold, roboto, sans-serif',
    fontSize: '20px',
    fontWeight: 700,
    height: '24px',
    lineHeight: '33.4px',
    margin: '0.5rem',
  },
  subtitle: {
    color: '#000000',
    fontFamily:
      'helvetica-w01-roman, helvetica-w02-roman, helvetica-lt-w10-roman, sans-serif',
    fontSize: '14px',
    fontWeight: 400,
    // height: '65px',
    lineHeight: '25.06px',
    // maxWidth: '35vw',
  },
  gridItem: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    padding: '1rem 15rem',
    margin: '2rem 0rem',
  },
  aboutUs: {
    color: '#f1a104',
    fontFamily: 'roboto-bold, roboto, sans-serif',
    fontSize: '30px',
    fontWeight: 700,
    height: '36px',
    lineHeight: 'normal',
    width: '100%',
    textAlign: 'center',
  },

  NNCharminar: {
    width: '100%',
    height: '35vh',
    marginTop: '16px',
  },

  NNGuy: {
    width: '100%',
    height: '35vh',
    margin: '16px',
  },
});

const AboutUsSectionMobile = () => {
  const classes = useStyles();
  return (
    <Container style={{ padding: '3rem 0rem' }}>
      <Box className={classes.aboutUs}>About Us</Box>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} className={classes.gridItem}>
          <div className={classes.NNCharminar}>
            <Image
              src="/illustrations/NN_Charminar.svg"
              alt="/illustrations/NN_Charminar.svg"
              layout="responsive"
              width={500}
              height={500}
            />
          </div>
          <Box className={classes.title}>Discover your city!</Box>
          <Box className={classes.subtitle}>
            We think you need to be more updated about your local news and city just like
            you get updated on National and State News. We chose mail for our
            communication and 'You Only Need One Email' (YONO - E) per day strategy.
          </Box>
        </Grid>
        <Grid item xs={12} md={6} className={classes.gridItem}>
          <div className={classes.NNGuy}>
            <Image
              src="https://res.cloudinary.com/nearbynews/image/upload/v1608033280/Dont%20delete/NN_Guy_gy4pqx.png"
              alt="/illustrations/NN_Guy.svg"
              layout="fill"
            />
          </div>

          <Box className={classes.title}>Get back to good old habits</Box>
          <Box className={classes.subtitle}>
            Remember the experience of sipping a coffee and reading a local newspaper in
            the early morning? We would like to bring back the experience for you with a
            digital touch and zero spam.
          </Box>
        </Grid>
        <Grid item xs={12} md={6} className={classes.gridItem}>
          <Image
            src="/illustrations/NN_fam.svg"
            alt="/illustrations/NN_fam.svg"
            width="500px"
            height="300px"
          />
          <Box className={classes.title}>Political Mouthpieces</Box>
          <Box className={classes.subtitle}>
            Gone are those days when we encouraged children to watch a news channel for
            knowledge. Newspapers have become mouthpieces of political parties and News
            Rooms are new-age forums for propaganda and abusing each other. News Websites
            have become aggregators of gossips and sexually sleazy content. We report
            facts, not propaganda
          </Box>
        </Grid>
        <Grid item xs={12} md={6} className={classes.gridItem}>
          <Image
            src="/illustrations/NN_gang.svg"
            alt="/illustrations/NN_gang.svg"
            width="500px"
            height="300px"
          />
          <Box className={classes.title}>Team</Box>
          <Box className={classes.subtitle}>
            We are young and motivated team of hustlers with a mission to redefine the
            news consumption experience and an ambition to make inroads into the capital
            intensive mainstream media by disruptive use of technology. nearby news is a
            venture started by a group of Columbia, IIT, IIM and AshokaU Alumni.
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
};

export default AboutUsSectionMobile;
