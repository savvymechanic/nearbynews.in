export const notFoundImage = '/images/not-found.svg';
export const darkGGLogo = '/images/darkLogo.png';
export const lightGGLogo = '/images/lightLogo.png';
