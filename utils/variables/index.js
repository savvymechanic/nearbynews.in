export const themeStorage = 'theme';
export const userStorage = 'user';
export const yourLocationStorage = 'yourLocation';
export const yourBreadcrumbStorage = 'breadcrumb';

export const customThemeStorage = 'customTheme';
export const alertSlugStorage = 'alertSlug';

export const applyLinkTheme = 'applyLinkTheme';

export const drawerWidth = 260;

export const company = {
  title: 'Guardian Group',
  url: 'http://myguardiangroup.com',
  domain: 'myguardiangroup.com',
  slug: 'guardian-group',
  twitterHandler: 'guardiangrouptnt',
  facebookHandler: 'guardiangrouptnt',
  logo: '/images/lightLogo.png',
  description:
    'Insurance & Financial Service Company throughout the English and Dutch Caribbean',
  longDescription: `We put your needs at the centre of our financial planning and 
    asset management services and reliably deliver a consistent product, 
    which reaffirms our title of market leader in a seemingly saturated financial services industry`,
};

export const CMS_URL = 'http://localhost:1337'; // process.env.NEXT_PUBLIC_STRAPI_DOMAIN;
export const C360_ROUTER_URL = process.env.NEXT_PUBLIC_ROUTER_DOMAIN; // 'https://c360-core.azure-api.net';
export const C360_PWA_URL = process.env.NEXT_PUBLIC_PWA_DOMAIN;
export const timeOut = 150000;

export const APP_NAME = `Guardian Group`;

export const DEFAULT_LIMIT = 20;

export const apiCredentials = {
  subscriptionKey: '3d0934c2c0b741dc91b70118a3cd4705', // '84e7f71970654ef5ac531250e603ee70',
};
