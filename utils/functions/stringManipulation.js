export const removeAllSpaces = (string) => {
  if (!string) {
    return '';
  }
  const newString = string.split(' ').join('');
  // console.log('newString: ', newString);
  return newString;
};

export const toTitleCase = (str) => {
  return str?.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
};

export const uuidv4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = (Math.random() * 16) | 0; // eslint-disable-line no-bitwise

    const v = c === 'x' ? r : (r & 0x3) | 0x8; // eslint-disable-line no-bitwise
    return v.toString(16);
  });
};

export const toPascalCase = (str) => {
  return str
    ?.replace(/\w+/g, (w) => {
      return w[0].toUpperCase() + w.slice(1).toLowerCase();
    })
    .replace(/-/g, ' ');
};
