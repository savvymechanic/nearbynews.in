import { parseBool } from './boolManipulation';

export const nFormatter = (num, digits) => {
  const sign = Math.sign(num);
  const numAbs = Math.abs(num);
  const si = [
    { value: 1, symbol: '' },
    { value: 1e3, symbol: 'k' },
    { value: 1e6, symbol: 'M' },
    { value: 1e9, symbol: 'G' },
    { value: 1e12, symbol: 'T' },
    { value: 1e15, symbol: 'P' },
    { value: 1e18, symbol: 'E' },
  ];
  const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  let i;
  for (i = si.length - 1; i > 0; i--) {
    if (numAbs >= si[i].value) {
      break;
    }
  }
  const reverseNum = sign >= 0 ? Math.abs(numAbs) : -Math.abs(numAbs);
  const result =
    (reverseNum / si[i].value).toFixed(digits).replace(rx, '$1') + si[i].symbol;
  return result;
};

export const formatDataTableValue = ({
  inputValue,
  isNumeric = false,
  isCurrency = false,
  isPercent = false,
  isLocalize = false,
  isAbbrev = false,
  isBoolean = false,
  units,
  showOriginalUnits = false,
  showDataUnits = false,
}) => {
  let dataValue =
    isNumeric && !Number.isNaN(inputValue) ? parseFloat(inputValue) : inputValue;

  let originalValue = isLocalize
    ? dataValue.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      })
    : dataValue;

  originalValue = isCurrency ? `$${originalValue}` : originalValue;
  originalValue = isPercent ? `${originalValue}%` : originalValue;
  originalValue =
    units && showOriginalUnits ? `${originalValue} ${units}` : originalValue;
  originalValue = isBoolean ? parseBool(originalValue) : originalValue;

  dataValue =
    isLocalize && !isAbbrev
      ? dataValue.toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })
      : dataValue;
  dataValue = isAbbrev ? nFormatter(dataValue) : dataValue;
  dataValue = isCurrency ? `$${dataValue}` : dataValue;
  dataValue = isPercent ? `${dataValue}%` : dataValue;
  dataValue = units && showDataUnits ? `${dataValue} ${units}` : dataValue;
  dataValue = isBoolean ? parseBool(dataValue) : dataValue;

  return { dataValue, originalValue };
};
