import { getEntries } from './arrayManipulation';

export function isValidHttpUrl(string) {
  let url;

  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }

  return url.protocol === 'http:' || url.protocol === 'https:';
}

export const createUrl = ({
  scheme = window.location.protocol,
  server = window.location.hostname,
  port = window.location.port,
  path = window.location.pathname,
  queryObject,
}) => {
  let url = `${scheme}://${server}:${port}/${path}`;
  const param = new URLSearchParams(getEntries(queryObject)).toString();
  if (param) url += `?${param}`;
  return url;
};

export const createRelativeUrl = ({ path = window.location.pathname, queryObject }) => {
  let url = `${path}`;
  const param = new URLSearchParams(getEntries(queryObject)).toString();
  if (param) url += `?${param}`;
  return url;
};
