import { parseBool } from './boolManipulation';
import { getRef } from './objectManipulation';

export const fetchData = (
  apiCall,
  callAction,
  state,
  additionalParams = {},
  disablePending = false,
) => {
  if (!apiCall) return Promise.resolve(null);
  // console.log('apiCall: ', apiCall);
  const newParams = apiCall?.params?.reduce((acc, curObj) => {
    const key = curObj?.key;
    const reducerKey = curObj?.reducerKey;
    const isBoolean = curObj?.isBoolean;
    const isNumeric = curObj?.isNumeric;
    let staticValue = curObj?.staticValue;
    let reducerValue = getRef(state, reducerKey);
    if (isBoolean) {
      staticValue = parseBool(staticValue);
      reducerValue = parseBool(reducerValue);
    } else if (isNumeric) {
      staticValue = !Number.isNaN(staticValue) ? parseFloat(staticValue) : staticValue;
      reducerValue = !Number.isNaN(reducerValue)
        ? parseFloat(reducerValue)
        : reducerValue;
    }

    const b = {
      [key]: staticValue || reducerValue,
    };
    return Object.assign(acc, b);
  }, {});
  // console.log('newParams: ', newParams);
  const savedKey = apiCall?.savedKey;
  const mergedParams = { ...newParams, ...additionalParams };
  // console.log('mergedParams: ', mergedParams);
  return callAction(
    apiCall?.url,
    mergedParams,
    apiCall?.httpMethod,
    savedKey,
    disablePending,
  );
};

export const getAllData = (apiCalls, callAction, state) => {
  if (!apiCalls) return Promise.resolve(null);
  return Promise.all(apiCalls?.map((x, index) => fetchData(x, callAction, state)));
};
