const hexColorToRGB = function (hexColor) {
  const detectShorthand = /^#?([a-f\d])([a-f\d])([a-f\d])$/i; // #000 vs #000000
  hexColor = hexColor.replace(detectShorthand, function (m, r, g, b) {
    return r + r + g + g + b + b;
  });

  const hex_array = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexColor); // #000000 to #ffffff
  return hex_array
    ? {
        r: parseInt(hex_array[1], 16), // 0-255
        g: parseInt(hex_array[2], 16), // 0-255
        b: parseInt(hex_array[3], 16), // 0-255
      }
    : null;
};

export const hexToRGBAlpha = function (_hexColor, alpha) {
  // console.log("_hexColor: ", _hexColor);
  const hexColor = _hexColor ? _hexColor.toString() : '#fff';
  const rgb = hexColorToRGB(hexColor);

  return rgb ? `rgba(${rgb.r},${rgb.g},${rgb.b},${alpha})` : '';
};

export const shadeColor = (color, percent) => {
  const f = parseInt(color?.slice(1), 16);
  const t = percent < 0 ? 0 : 255;
  const p = percent < 0 ? percent * -1 : percent;
  const R = f >> 16;
  const G = (f >> 8) & 0x00ff;
  const B = f & 0x0000ff;
  return `#${(
    0x1000000 +
    (Math.round((t - R) * p) + R) * 0x10000 +
    (Math.round((t - G) * p) + G) * 0x100 +
    (Math.round((t - B) * p) + B)
  )
    .toString(16)
    .slice(1)}`;
};
