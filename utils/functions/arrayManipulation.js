export const groupBy = (list, keyGetter) => {
  const map = new Map();
  list?.forEach((item) => {
    const key = keyGetter(item);
    const collection = map?.get(key);
    if (!collection) {
      map?.set(key, [item]);
    } else {
      collection?.push(item);
    }
  });
  return map;
};

export const getEntries = (o = {}) => {
  const entries = [];
  for (const [k, v] of Object?.entries(o)) {
    if (Array?.isArray(v)) entries.push(...v?.flatMap(getEntries));
    else if (typeof v === 'object') entries?.push(...getEntries(v));
    else entries?.push([k, v]);
  }
  return entries;
};

export function arrayBufferToBase64(buffer) {
  let binary = '';
  const bytes = new Uint8Array(buffer);
  const len = bytes?.byteLength;
  for (let i = 0; i < len; i++) {
    binary += String?.fromCharCode(bytes[i]);
  }
  return window.btoa(binary);
}

export function base64ToArrayBuffer(base64) {
  const binary_string = window.atob(base64);
  const len = binary_string.length;
  const bytes = new Uint8Array(len);
  for (let i = 0; i < len; i++) {
    bytes[i] = binary_string?.charCodeAt(i);
  }
  return bytes?.buffer;
}

export function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - (base64String?.length % 4)) % 4);
  const base64 = (base64String + padding)?.replace(/-/g, '+').replace(/_/g, '/');
  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData?.length);
  for (let i = 0; i < rawData?.length; ++i) {
    outputArray[i] = rawData?.charCodeAt(i);
  }
  return outputArray;
}

export const removeDuplicates = (array, key) => {
  return array?.reduce((arr, item) => {
    const removed = arr?.filter((i) => i[key] !== item[key]);
    return [...removed, item];
  }, []);
};

export const addItemOnce = (arr, item, key) => {
  if (!item) return arr;
  let index = -1;
  if (key) index = arr?.findIndex((oneItem) => oneItem[key] === item[key]);
  else index = arr?.findIndex((oneItem) => oneItem === item);

  if (index > -1) {
    arr?.splice(index, 1, item);
  } else {
    arr?.push(item);
  }
  return arr;
};

export const removeItemOnce = (arr, item, key) => {
  if (!item) return arr;
  let index = -1;
  if (key) index = arr?.findIndex((oneItem) => oneItem[key] === item[key]);
  else index = arr?.findIndex((oneItem) => oneItem === item);

  if (index > -1) {
    arr?.splice(index, 1);
  }
  return arr;
};
