export const renameKeys = (keysMap, obj) =>
  Object.keys(obj).reduce(
    (acc, key) => ({
      ...acc,
      ...{ [keysMap[key] || key]: obj[key] },
    }),
    {},
  );

export function compareAsc(a, b, key) {
  // Use toUpperCase() to ignore character casing
  const keyA = a[key].toLowerCase();
  const keyB = b[key].toLowerCase();

  let comparison = 0;
  if (keyA > keyB) {
    comparison = 1;
  } else if (keyA < keyB) {
    comparison = -1;
  }
  return comparison;
}

/*
var obj = { a: { b: 1, c : { d : 3, e : 4}, f: 5 } }
str = 'a.c.d'
ref(obj, str) // 3
*/
export function getRef(obj, str) {
  if (!str) return null;
  return str.split('.').reduce(function (o, x) {
    return !!o && o[x];
  }, obj);
}

/*
    var obj = { a: { b: 1, c : { d : 3, e : 4}, f: 5 } }
    str = 'a.c.d'
    set(obj, str, 99)
    console.log(obj.a.c.d) // 99
    */
export function setRef(obj, str, val) {
  if (!str) return null;
  str = str.split('.');
  while (str.length > 1) obj = obj[str.shift()];
  return (obj[str.shift()] = val);
}

export function deepen(obj) {
  const result = {};

  for (const objectPath in obj) {
    const parts = objectPath.split('.');

    let target = result;
    while (parts.length > 1) {
      const part = parts.shift();
      target = target[part] = target[part] || {};
    }

    target[parts[0]] = obj[objectPath];
  }

  return result;
}
/*
    // For example ...
    console.log(deepen({
      'ab.cd.e': 'foo',
      'ab.cd.f': 'bar',
      'ab.g': 'foo2'
    }));
    {
      "ab": {
        "cd": {
          "e": "foo",
          "f": "bar"
        },
        "g": "foo2"
      }
    }
    */

export const sourceTextFromData = (state, str) => {
  // const url = 'https://url.com/{{query}}/foo/{{query2}}';
  // const result = { query: 'queryResult1', query2: 'queryResult2' };
  const newString = str?.replace(/{{(.+?)}}/g, (_, g1) => getRef(state, g1) || '?') || '';
  // console.log('newString: ', newString);
  return newString;
};
